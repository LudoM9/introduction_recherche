% CLASSE DU DOCUMENT
\documentclass[9pt,xcolor={svgnames},]{beamer}

\usepackage{ensta_beamer_2019}
\RequirePackage[english]{babel}
\usepackage{subfig}
\usepackage{tikz}
\usepackage{tabularray} % for tables
\usepackage{hyperref} % Liens dans le pdf
\usepackage{xcolor}

\usetikzlibrary{shapes.geometric, shapes.misc, tikzmark}
\usetikzlibrary{positioning}


\newcommand\setItemnumber[1]{\setcounter{enumi}{\numexpr#1-1\relax}}

% A DECOMMENTER SI ON VEUT FAIRE 2 slides sur une page
% \pgfpagesuselayout{2 on 1}[a4paper,border shrink=5mm]


% TITLEPAGE
\title[Transformer: A Novel Neural Network Architecture]{\textbf{Initiation à la Recherche: \\
Transformer: A Novel Neural Network Architecture}}
%\subtitle[]{Computer-assisted seafloor interpretation from Side Scan Sonar data}
\author[Ludovic Mustiere]{\textbf{Ludovic Mustiere}}
\date[version \today]{2023 - 2024}
\institute[ENSTA Bretagne]{ENSTA Bretagne}

% ---------------------------------------------------------------------------------------------------- BEGIN DOCUMENT
\begin{document}

% TITLE PAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
    \titlepage
    \begin{block}{}
        \centering
        \Huge
        Initiation à la Recherche:\\
        \large
        \vspace{4pt}
        Transformer: A Novel Neural Network Architecture \\
        \vspace{4pt}
        ENSTA Bretagne
    \end{block}
    \centering
    \vspace{25pt}
    Ludovic MUSTIERE\\
    \vspace{25pt}
    \today
\end{frame}

\begin{frame}{Contents}
    \tableofcontents[sectionstyle=show,subsectionstyle=hide]
\end{frame}

% \Introduction to Neural Networks
\begin{frame}{Introduction to Neural Networks}
    \begin{alertblock}{NN}
        \begin{itemize}
            \item Neural Networks (NN) are a class of algorithms inspired by the human brain.
            \item They are used to recognize patterns.
            \item They interpret sensory data through a kind of machine perception, labeling or clustering raw input.
        \end{itemize}
    \end{alertblock}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{images/FNN.png}
        \captionsetup{justification=centering}
        \caption{A Feedforward Neural Network}
        \label{fig:fnn}
    \end{figure}
\end{frame}

% \FNN, RNN and LSTM
\begin{frame}{Feedforward Neural Network (FNN), Recurrent Neural Network (RNN) and Long Short-Term Memory (LSTM)}
    \begin{columns}
        \column{0.5\textwidth}
        \begin{block}{FNN}
            \begin{itemize}
                \item The simplest form of a neural network.
                \item The information moves in only one direction: forward.
                \item It is used for simple classification tasks.
            \end{itemize}
        \end{block}
        \begin{block}{RNN}
            \begin{itemize}
                \item It has connections creating loops in the network.
                \item It is used for tasks that require the network to have some form of memory.
            \end{itemize}
        \end{block}
        \column{0.5\textwidth}
        \begin{block}{LSTM}
            \begin{itemize}
                \item It is a special kind of RNN.
                \item It is capable of learning long-term dependencies.
                \item It is used for tasks that require the network to have some form of memory.
            \end{itemize}
        \end{block}
    \end{columns}
\end{frame}

% \Issues with RNN and LSTM
\begin{frame}{Issues with RNN and LSTM}
    \begin{alertblock}{Issues with RNN and LSTM}
        \begin{itemize}
            \item RNN and LSTM have a sequential nature.
            \item They are not able to capture the dependencies between words in a sentence.
            \item They are not able to parallelize the computation.
        \end{itemize}
    \end{alertblock}
    \begin{figure}
        \centering
        \includegraphics[width=0.5\linewidth]{images/RNN-unrolled.png}
        \captionsetup{justification=centering}
        \caption{A Recurrent Neural Network}
        \label{fig:rnn}
    \end{figure}
\end{frame}

% \Attention
\begin{frame}{Attention}
    \begin{alertblock}{Attention}
        \begin{itemize}
            \item Attention is a mechanism that allows a model to focus on the most relevant parts of the input.
            \item It is used in tasks such as machine translation, image captioning, and speech recognition.
        \end{itemize}
    \end{alertblock}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\linewidth]{images/attention_sequence.png}
        \captionsetup{justification=centering}
        \caption{\href{https://jalammar.github.io/images/seq2seq_7.mp4}{An Attention Mechanism}}
        \label{fig:attention}
    \end{figure}
\end{frame}

% \Attention
\begin{frame}{Attention}
    \begin{block}{Attention}
        \begin{itemize}
            \item Global Attention: The attention mechanism looks at all the encoder states.
            \item Local Attention: The attention mechanism looks at a subset of the encoder states.
            \item It computes a score for each state.
            \item It computes a weighted sum of the encoder states.
        \end{itemize}
    \end{block}
    \begin{equation}\label{eq:attentional_vector}
        \tilde{h}_t = \tanh(W_c[h_t; c_t])
    \end{equation}
    \begin{equation}\label{eq:output}
        p(y_t | y_{<t}, x) = \text{softmax}(W_s\tilde{h}_t)
    \end{equation}
    \begin{block}{}
        \begin{itemize}
            \item $h_t$ is the hidden state of the decoder at time step $t$.
            \item $c_t$ is the context vector at time step $t$.
            \item $W_c$ and $W_s$ are the weights.
        \end{itemize}        
    \end{block}
\end{frame}

% \Global Attention
\begin{frame}{Global Attention}
    \begin{block}{Global Attention}
        \begin{itemize}
            \item The attention mechanism looks at all the encoder states.
        \end{itemize}
    \end{block}
    \begin{equation}\label{eq:alignment_vector_detail}
        a_t(s) = \frac{\exp(\text{score}(h_t, \bar{h}_s))}{\sum_{s'} \exp(\text{score}(h_t, \bar{h}_{s'}))}
    \end{equation}
    \begin{equation}\label{eq:context_vector}
        c_t = \sum_s a_t(s)\bar{h}_s
    \end{equation}
    \begin{figure}
        \centering
        \includegraphics[width = 0.35\textwidth,keepaspectratio]{images/global_attention_model.png}
        \captionsetup{justification=centering}
        \caption{Global Attention Model}
        \label{fig:global_attention_model}
    \end{figure}
\end{frame}


% \Local Attention
\begin{frame}{Local Attention}
    \begin{block}{Local Attention}
        \begin{itemize}
            \item The attention mechanism looks at a subset of the encoder states.
        \end{itemize}
    \end{block}
    \begin{equation}\label{eq:aligned_position}
        p_t = S \cdot \text{sigmoid}(v_p^T\tanh(W_ph_t))
    \end{equation}
    \begin{equation}\label{eq:alignment_vector_local}
        a_t(s) = align(h_t, \bar{h}_s) \cdot \text{exp}(-\frac{(s - p_t)^2}{2\sigma^2})
    \end{equation}
    \begin{figure}
        \centering
        \includegraphics[width = 0.35\textwidth,keepaspectratio]{images/local_attention_model.png}
        \caption{Local attention model}
        \label{fig:local_attention_model}
    \end{figure}
\end{frame}

% \Transformer
\begin{frame}{Transformer}
    \begin{block}{Transformer}
        \begin{itemize}
            \item The Transformer is a novel neural network architecture.
            \item It is based on the attention mechanism.
            \item It is able to capture the dependencies between words in a sentence.
            \item It is able to parallelize the computation.
        \end{itemize}
    \end{block}
    \begin{block}{Architecture}
        \begin{itemize}
            \item The Transformer architecture consists of an encoder and a decoder consisting of a stack of $N$ identical layers.
            \item The encoder processes the input and produces a set of encoder states.
            \item The decoder processes the encoder states and produces the output.
            \item Each layer has two sub-layers: a multi-head self-attention mechanism and a position-wise fully connected feed-forward network.
            \item The decoder consists of a stack of $N$ identical layers.
            \item Each layer has three sub-layers: a multi-head self-attention mechanism, a multi-head attention mechanism, and a position-wise fully connected feed-forward network.
        \end{itemize}
    \end{block}
\end{frame}

% \Transformer
\begin{frame}{Transformer}
    \begin{figure}
        \centering
        \includegraphics[height=0.8\textheight]{images/transformer_architecture.png}
        \caption{The Transformer architecture}
        \label{fig:transformer_architecture}
    \end{figure}
\end{frame}

% \Positional Encoding
\begin{frame}{Positional Encoding}
    \begin{block}{Positional Encoding}
        \begin{itemize}
            \item The Transformer does not have any notion of the order of the words in the input sequence.
            \item Positional encoding is added to the input embeddings to give the model some information about the relative or absolute position of the tokens in the sequence.
        \end{itemize}
    \end{block}
    \begin{equation}\label{eq:positional_encoding_sin}
        PE_{(pos, 2i)} = \sin(pos/10000^{2i/d_{model}})
    \end{equation}
    \begin{equation}\label{eq:positional_encoding_cos}
        PE_{(pos, 2i+1)} = \cos(pos/10000^{2i/d_{model}})
    \end{equation}
\end{frame}

% \Multi-Head Attention
\begin{frame}{Multi-Head Attention}
    \begin{figure}
        \centering
        \includegraphics[width = 0.8\textwidth,keepaspectratio]{images/head_attention.png}
        \caption{Multi-Head Attention}
        \label{fig:multi_head_attention}
    \end{figure}
\end{frame}

% \Multi-Head Attention
\begin{frame}{Multi-Head Attention}
    \begin{block}{Multi-Head Attention}
        \begin{itemize}
            \item Multi-head attention allows the model to focus on different parts of the input sequence.
            \item It is used to compute the attention mechanism in parallel.
        \end{itemize}
    \end{block}
    \begin{equation}\label{eq:attention_score}
        \text{Attention}(Q, K, V) = \text{softmax}\left(\frac{QK^T}{\sqrt{d_k}}\right)V
    \end{equation}
    \begin{equation}\label{eq:attention_softmax}
        \text{softmax}(x_i) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}
    \end{equation}
    \begin{equation}\label{eq:attention_output}
        \text{MultiHead}(Q, K, V) = \text{Concat}(\text{head}_1, \text{head}_2, ..., \text{head}_h)W^O
    \end{equation}
\end{frame}

% \Training and Results
\begin{frame}{Training and Results}
    \begin{block}{Basic Model Training}
        \begin{itemize}
            \item Trained on the WMT 2014 English-to-French translation.
            \item The model was trained for 100,000 steps.
            \item The model was trained on 8 NVIDIA P100 GPUs.
            \item It took 12 hours to train the model.
        \end{itemize}
    \end{block}
    \begin{block}{Big Model Training}
        \begin{itemize}
            \item The model was trained for 300,000 steps.
            \item It took 3.5 days to train the model.
        \end{itemize}
    \end{block}
\end{frame}

% \Training and Results
\begin{frame}{Training and Results}
    \begin{block}{Results}
        \begin{itemize}
            \item The model achieved a BLEU score of 41.8 on the WMT 2014 English-to-French translation.
            \item The model achieved a BLEU score of 38.4 on the WMT 2014 English-to-German translation.
        \end{itemize}
    \end{block}
    \begin{figure}
        \centering
        \includegraphics[width = \textwidth, height=.5\textheight,keepaspectratio]{images/english_to_french.png}
        \caption{Results of the Transformer model}
        \label{fig:transformer_results}
    \end{figure}
\end{frame}

% \Conclusion
\begin{frame}{Conclusion}
    \begin{block}{Conclusion}
        \begin{itemize}
            \item The Transformer model outperformed the previous state-of-the-art models.
            \item The Transformer model is able to capture the dependencies between words in a sentence.
            \item The Transformer model is able to parallelize the computation.
        \end{itemize}
    \end{block}
\end{frame}

% \Annexe 1 : Attention compute
\begin{frame}{Annex 1 : Attention Computation}
    \begin{block}{Attention}
        \begin{itemize}
            \item Look at the set of encoder states and compute a score for each state.
            \item Compute a weighted sum of the encoder states.
        \end{itemize}
    \end{block}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\linewidth]{images/attention_compute.png}
        \captionsetup{justification=centering}
        \caption{\href{https://jalammar.github.io/images/attention_process.mp4}{Attention Process}}
        \label{fig:attention_compute}
    \end{figure}
\end{frame}

% \Annexe 2 : Attention RNN
\begin{frame}{Annex 2 : Attention in RNN}
    \begin{block}{Attention in RNN}
        \begin{itemize}
            \item Attention Decoder RNN takes an initial hidden state and process a new hidden state vector $h4$.
            \item Calculate context vector $c4$ using the attention mechanism.
            \item Combine $c4$ and $h4$ to produce the final output and passes it to FNN.
            \item Send the output to the next time step.
        \end{itemize}
    \end{block}
    \begin{figure}
        \centering
        \includegraphics[width=0.7\linewidth]{images/attention_RNN.png}
        \captionsetup{justification=centering}
        \caption{\href{https://jalammar.github.io/images/attention_tensor_dance.mp4}{Attention in RNN}}
        \label{fig:attention_rnn}
    \end{figure}
\end{frame}

% \Annexe 3 : Self Attention Output
\begin{frame}{Annex 3 : Self Attention Output}
    \begin{figure}
        \centering
        \includegraphics[height=0.8\textheight,keepaspectratio]{images/self-attention-output.png}
        \caption{Self Attention Output}
        \label{fig:self_attention_output}
    \end{figure}
\end{frame}

% \Annexe 4 : Multi-Head Attention
\begin{frame}{Annex 4 : Multi-Head Attention}
    \begin{figure}
        \centering
        \includegraphics[width = 0.8\textwidth,keepaspectratio]{images/transformer_multi-headed_self-attention-recap.png}
        \caption{Multi-Head Attention}
        \label{fig:multi_head_attention_annex}
    \end{figure}
\end{frame}

% \Annexe 5 : Visualization
\begin{frame}{Annex 5 : Visualization of the Transformer}
    \begin{figure}
        \centering
        \includegraphics[height=0.8\textheight,keepaspectratio]{images/visualisation_matrix.png}
        \caption{Visualization of the Transformer}
        \label{fig:transformer_visualization}
    \end{figure}
\end{frame}

% \Annexe 6 : Visualization
\begin{frame}{Annex 6 : Visualization of the Transformer}
    \begin{figure}
        \centering
        \includegraphics[width = 0.9\textwidth,keepaspectratio]{images/visualisation.png}
        \caption{Visualization of the Transformer}
        \label{fig:transformer_visualization_2}
    \end{figure}
\end{frame}

% \Annexe 7 : Visualization
\begin{frame}{Annex 7 : Visualization of the Transformer}
    \begin{figure}
        \centering
        \includegraphics[width = 0.8\textwidth,keepaspectratio]{images/visualisation2.png}
        \caption{Visualization of the Transformer}
        \label{fig:transformer_visualization_3}
    \end{figure}
\end{frame}

\end{document}