% CLASSE DU DOCUMENT
\documentclass[9pt,
xcolor={svgnames},
%hyperref={colorlinks,citecolor=DarkRed,linkcolor=DarkRed,urlcolor=DarkRed}
]{beamer}


\usepackage{ensta_beamer_2019}
\RequirePackage[greek, english]{babel}
\usepackage{subfig}
\usepackage{tikz}
\usepackage{tabularray} % for tables
\usepackage{hyperref} % Liens dans le pdf
\usepackage{xcolor}
\usepackage{minted}

\usetikzlibrary{shapes.geometric, shapes.misc, tikzmark}
\usetikzlibrary{positioning}


\newcommand\setItemnumber[1]{\setcounter{enumi}{\numexpr#1-1\relax}}

% A DECOMMENTER SI ON VEUT FAIRE 2 slides sur une page
% \pgfpagesuselayout{2 on 1}[a4paper,border shrink=5mm]


% TITLEPAGE
\title[Engineering Assistant Internship]{\textbf{Engineering Assistant Internship: \\
Conception and Control of an Autonomous Sailboat}}
%\subtitle[]{Computer-assisted seafloor interpretation from Side Scan Sonar data}
\author[Ludovic Mustiere]{\textbf{Ludovic Mustiere}}
\date[version \today]{2023 - 2024}
\institute[ENSTA Bretagne]{ENSTA Bretagne}

% ---------------------------------------------------------------------------------------------------- BEGIN DOCUMENT
\begin{document}
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
    \titlepage
    \begin{block}{}
        \centering
        \Huge
        Engineering Assistant Internship:\\
        \large
        \vspace{4pt}
        Conception and Control of an Autonomous Sailboat \\
        \vspace{4pt}
        ENSTA Bretagne\\
        College of Engineering and Physical Sciences, Aston University
    \end{block}
    \centering
    \vspace{25pt}
    Ludovic MUSTIERE\\
    \vspace{25pt}
    \today
\end{frame}

\begin{frame}{Contents}
    \tableofcontents[sectionstyle=show,subsectionstyle=hide]
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Context}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{Some History first}
\begin{frame}{Some History first}
    \begin{alertblock}{Sailboats}
        Sailing boats have been used for thousands of years, and they are still used today. They are used for leisure, sport, and transport around the world.
    \end{alertblock}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{images/Le_Redoutable_a_Trafalgar.jpeg}
        \captionsetup{justification=centering}
        \caption{
            Louis-Philippe Crépin, \emph{Le Redoutable à Trafalgar}, 1807, Musée national de la Marine.
            \emph{The Redoutable} at the battle of Trafalgar, between the \emph{Victory} on the left and the
            \emph{Temeraire} on the right.
        }
        \label{fig:LeRedoutableatTrafalgar}
    \end{figure}
\end{frame}

% \subsection{Global Shipping}
\begin{frame}{Global Shipping}
    \begin{alertblock}{Shipping}
        Shipping is the \textbf{most efficient and cost-effective} method of international transportation for most goods. It is responsible for about 90\% of world trade.
    \end{alertblock}
    \begin{block}{}
        \begin{itemize}
            \item 100,000 vessels worldwide
            \item 10 billion tonnes in 2017
            \item Extremely harmful impact on the environment
        \end{itemize}
    \end{block}
    \begin{alertblock}{Sailboats}
        \textbf{Autonomous sailboats} could provide a solution to these problems.
    \end{alertblock}
\end{frame}

% \subsection{Autonomous Sailboat Project}
\begin{frame}{Autonomous Sailboat Project}
    \begin{block}{Objectives}
        \begin{itemize}
            \item Program the control algorithms
            \item Ensure the proper functioning of each of the sensors
            \item Develop a simulator
            \item Test in real conditions
        \end{itemize}
    \end{block}
    \begin{figure}[h!]
        \includegraphics[width=0.3\linewidth,height=.5\textheight,keepaspectratio]{images/Sailboat_Ragazza.png}
        \captionsetup{justification=centering}
        \caption{
            Ragazza 1 Meter Sailboat V2: RTR
        }
        \label{fig:Sailboat_Ragazza}
    \end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{System Architecture}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection{Hardware Architecture}
\begin{frame}{Hardware Architecture}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\linewidth,height=.7\textheight,keepaspectratio]{./images/Sailboat.png}
        \captionsetup{justification=centering}
        \caption{
            Ragazza 1 Meter Sailboat V2: RTR with his four main parts
        }
        \label{fig:Sailboat_Detailed}
    \end{figure}
\end{frame}

\begin{frame}{Hardware Architecture}
    \begin{block}{Objectives}
        \begin{itemize}
            \item Raspberry Pi 4
            \item Navio2 HAT
            \item RC Receiver
            \item Calypso Anemometer
            \item Hitec HS-785HB (Sail winch servo)
            \item Hitec HS-5645MG (Rudder servo)
        \end{itemize}
    \end{block}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.45\linewidth,height=.4\textheight,keepaspectratio]{./images/Rpi4andNavio2.png}
        \captionsetup{justification=centering}
        \caption{
            Navio2 HAT on top of a Raspberry Pi 4
        }
        \label{fig:Rpi4andNavio2}
    \end{figure}
\end{frame}

% \subsection{Software Architecture}
\begin{frame}{Software Architecture}
    \begin{table}[!ht]
        \centering
        \begin{tblr}{
        colspec = { |c|c|c| },
        row{1} = {cyan}
        }
            \hline
            \textbf{Library} & \textbf{Version} & \textbf{Description} \\
            \hline
            \hline
            \href{https://numpy.org/}{numpy} & 1.25.1 & Scientific computing \\
            \hline
            \href{https://www.scipy.org/}{scipy} & 1.11.1 & Scientific computing \\
            \hline
            \href{https://matplotlib.org/}{matplotlib} & 3.7.2 & Plotting library \\
            \hline
            \href{https://docs.python.org/3/library/tkinter.html}{tkinter} & 8.6 & GUI toolkit \\
            \hline
            \href{https://pypi.org/project/pygubu/}{pygubu} & 0.31 & GUI builder \\
            \hline
            \href{https://pypi.org/project/PyYAML/}{PyYAML} & 6.0.1 & YAML parser \\
            \hline
            \href{https://proj.org/}{pyproj} & 3.6.0 & Cartographic projection \\
            \hline
            \href{https://pypi.org/project/calypso-anemometer/}{calypsoanemometer} & 0.6.0 & Calypso Anemometer driver \\
            \hline
            \href{https://pypi.org/project/Navio2/}{Navio2} & 1.0.0 & Navio2 driver \\
            \hline
        \end{tblr}
    \caption{Python Libraries}
    \label{table:python_libraries}
    \end{table}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Drivers}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection{Modularity and Multithreading}
\begin{frame}{Modularity and Multithreading}
    \begin{block}{Modularity}
        \begin{itemize}
            \item Each sensor is a module
            \item Each module is a thread
            \item Each module is independent
        \end{itemize}
    \end{block}
    \begin{block}{Advantages}
        \begin{itemize}
            \item Easy to add and remove new sensors
            \item Can use the modules in other projects
            \item Fast and efficient
        \end{itemize}
    \end{block}
\end{frame}

% \subsection{Packages}
\begin{frame}{Packages}
    \begin{block}{Advantages}
        \begin{itemize}
            \item Easy import of the modules
            \item More readable code
        \end{itemize}
    \end{block}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.9\linewidth,height=.9\textheight,keepaspectratio]{./images/Package_code.png}
        \captionsetup{justification=centering}
        \caption{
            Drivers Package \texttt{\_\_init\_\_.py} File
        }
        \label{fig:Package_code}
    \end{figure}
\end{frame}

% \subsection{Calypso Anemometer}
\begin{frame}{Calypso Anemometer}
    \begin{block}{Calypso Anemometer}
        \begin{itemize}
            \item Ultrasonic wind sensor
            \item Measures wind speed and direction
        \end{itemize}
    \end{block}
    \begin{alertblock}{Major Issues}
        \begin{itemize}
            \item Bluetooth connection
            \item Solared powered
            \item Few documentation
        \end{itemize}
    \end{alertblock}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.5\linewidth,height=.35\textheight,keepaspectratio]{./images/Calypso.jpeg}
        \captionsetup{justification=centering}
        \caption{
            Calypso Anemometer
        }
        \label{fig:Calypso_Anemometer}
    \end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Control Algorithms}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection{True Wind and Apparent Wind}
\begin{frame}{True Wind and Apparent Wind}
    \begin{block}{True Wind}
        \begin{itemize}
            \item Wind speed and direction relative to the ground
        \end{itemize}
    \end{block}
    \begin{block}{Apparent Wind}
        \begin{itemize}
            \item Wind speed and direction relative to the sailboat
            \item Wind speed and direction measured by the Calypso Anemometer
            \item Sailboat speed and direction measured by the GPS and the IMU
        \end{itemize}
    \end{block}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\linewidth,height=.4\textheight,keepaspectratio]{./images/true-vs-apparent-wind.png}
        \captionsetup{justification=centering}
        \caption{
            True wind and apparent wind
        }
        \label{fig:TrueWindVsApparentWind}
    \end{figure}
\end{frame}

\begin{frame}{True Wind and Apparent Wind}
    \centering
    \begin{equation}
        \label{eq:TrueWindSpeed}
        V_{tw} = \sqrt{ ( V_{aw} * \cos(A))^2 + ( V_{aw} * \sin(A) + V_{s})^2 }
    \end{equation}
    
    \begin{equation}
        \label{eq:TrueWindDirection}
        \theta_{tw} = \arctan2(V_{aw} * \sin(A) + V_{s}, V_{aw} * \cos(A))
    \end{equation}
    
    \begin{block}{Variables}
        %List of variables:
        \begin{itemize}
            \item $V_{aw}$ the apparent wind speed
            \item $A$ the apparent wind angle
            \item $V_{s}$ the sailboat speed
            \item $V_{tw}$ the true wind speed
            \item $\theta_{tw}$ the true wind direction
        \end{itemize}
    \end{block}
\end{frame}

% \subsection{Tacking}
\begin{frame}{Tacking}
    \begin{block}{Tacking}
        \begin{itemize}
            \item Sailing maneuver to go upwind
            \item Zigzag pattern
            \item Often between 45 and 60 degrees
        \end{itemize}
    \end{block}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\linewidth,height=.55\textheight,keepaspectratio]{./images/tacking.jpeg}
        \captionsetup{justification=centering}
        \caption{
            Tacking
        }
        \label{fig:Tacking}
    \end{figure}
\end{frame}

% \subsection{Line Following and Station Keeping}
\begin{frame}{Line Following and Station Keeping}
    \begin{block}{Line Following}
        \begin{itemize}
            \item Follow a line
            \item \emph{A Simple Controller for Line Following of Sailboats} by \textsc{Luc Jaulin} and \textsc{Fabrice Le Bars}
        \end{itemize}
    \end{block}
    \begin{block}{Station Keeping}
        \begin{itemize}
            \item Reach a specific point
            \item Stay in a specific area
            \item \emph{Position keeping control of an autonomous sailboat} by \textsc{Christophe Viel}, \textsc{Ulysse Vautier}, \textsc{Jian Wan} and \textsc{Luc Jaulin}
        \end{itemize}
    \end{block}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\linewidth,height=.4\textheight,keepaspectratio]{./images/stationkeeping.png}
        \captionsetup{justification=centering}
        \caption{
            Steps of orientation control.
        }
        \label{fig:StationKeeping}
    \end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection{Model}
\begin{frame}{Model}
    \begin{table}[!ht]
        \centering
        \begin{tabular}{ |c|c||c|c| }
            \hline
            \textbf{Parameter} & \textbf{Value} & \textbf{Parameter} & \textbf{Value} \\
            \hline
            $p_{1}\ {\rm -}$ & Drift coefficient & $p_{7}\ {\rm \left[m\right]}$ & Distance to mast \\
            $p_{2}\ {\rm \left[kgs^{-1}\right]}$ & Tangential friction& $p_{8}\ {\rm \left[m\right]}$ & Distance to rudder \\
            $p_{3}\ {\rm \left[kgm\right]}$ & Angular friction & $p_{9}\ {\rm \left[kg\right]}$ & Mass of boat \\
            $p_{4}\ {\rm \left[kgs^{-1}\right]}$ & Sail lift & $p_{10}\ {\rm \left[kgm^{2}\right]}$ & Moment of inertia \\
            $p_{5}\ {\rm \left[kgs^{-1}\right]}$ & Rudder lift & $p_{11}\ {\rm -}$ & Rudder break coefficient \\
            $p_{6}\ {\rm \left[m\right]}$ & Distance to sail center of effort & & \\
            \hline
        \end{tabular}
        \caption{Model parameters}
        \label{tab:ModelParameters}
    \end{table}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\linewidth,height=.3\textheight,keepaspectratio]{./images/Model.png}
        \captionsetup{justification=centering}
        \caption{
            Left: Position ($x$, $y$) and orientation ($\theta$) of the sailboat is given in a North-
        East-Up reference frame. Right: Velocity ($v$) and angle of rudder ($\delta_{r}$ ) and the angle of
        the sail ($\delta_{s}$) is given in a sailboat fixed reference system together with distances ($p_{6}$ , $p_{7}$
        and $p_{8}$ ).
        }
        \label{fig:Model}
    \end{figure}
\end{frame}

\begin{frame}{Model}
    \begin{block}{Model}
        \begin{itemize}
            \item $\textbf{x}=[ x\ y\ \theta\ v\ \omega ]^T$
            \item $\dot{\textbf{x}} = f\left( \textbf{x},u,W_{p,tw}\right)$
            \item \emph{Modeling, control and state-estimation for an autonomous sailboat} by \textsc{Jon Melin}
        \end{itemize}
    \end{block}
    \begin{equation}
        \label{eq:StateEquationComplete}
        \begin{bmatrix}
            \dot{x} \\
            \dot{y} \\
            \dot{\theta} \\
            \dot{v} \\
            \dot{\omega}
        \end{bmatrix}
        =
        \begin{bmatrix}
            v\cos(\theta) + p_{1}a_{tw}\cos(\psi_{tw}) \\
            v\sin(\theta) + p_{1}a_{tw}\sin(\psi_{tw}) \\
            \omega \\
            \left(g_{s}\sin(\delta_{s}) - g_{r}p_{11}\sin(\delta_{r}) - p_{2}v^{2}\right)/p_{9} \\
            \left(g_{s}\left(p_{6} - p_{7}\cos(\delta_{s})\right) - g_{r}p_{8}\cos(\delta_{r}) - p_{3}v\omega\right)/p_{10}
        \end{bmatrix}
    \end{equation}
\end{frame}

\begin{frame}{Graphical User Interface}
    \begin{block}{Model}
        \begin{itemize}
            \item Human-friendly
            \item Easy change of the parameters of the simulation
        \end{itemize}
    \end{block}
    \begin{figure}[ht]
        \centering
        \includegraphics[width = \textwidth, height=.6\textheight,keepaspectratio]{./images/Simulation_window.png}
        \caption{Simulation window}
        \label{fig:SimulationWindow}
    \end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Test and Results}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection{Test}
\begin{frame}{Location}
    \begin{block}{Test}
        \begin{itemize}
            \item \texttt{Bournville Radio Sailing and Model Boat Club}
            \item Test small scale models
            \item Light wind conditions
        \end{itemize}
    \end{block}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\linewidth,height=.5\textheight,keepaspectratio]{./images/Lake.jpg}
        \captionsetup{justification=centering}
        \caption{
            Bournville artificial lake.
        }
        \label{fig:Lake}
    \end{figure}
\end{frame}

% \subsection{Results}
\begin{frame}{Results}
    \begin{block}{Line Following}
        \begin{itemize}
            \item Reach the line and follow it for around 8 meters
            \item Issues with the wind vector
            \item Need to filter the Calypso anemometer results
        \end{itemize}
    \end{block}
    \begin{block}{Station Keeping}
        \begin{itemize}
            \item Not concluant
            \item IMU issues (lot of induced current)
            \item Raspberry Pi 4 issues (not enough power)
        \end{itemize}
    \end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection{Conclusion}
\begin{frame}{Conclusion}
    \begin{block}{Conclusion}
        \begin{itemize}
            \item Control algorithms work in simulation
            \item Line Following works in real conditions
            \item Station Keeping does not work in real conditions
            \item All work is documented
        \end{itemize}
    \end{block}
    \begin{block}{Future Work}
        \begin{itemize}
            \item Modify the configuration inside the sailboat
            \item Fix the battery inside the sailboat
            \item Improve the simulation
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \centering
    \Huge
    Thank you for your attention.\\
    \vspace{1cm}
    Do you have questions?
\end{frame}


\end{document}