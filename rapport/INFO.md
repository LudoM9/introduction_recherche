# Introduction à la recherche: Chat GPT et les transformers

## Plan pour le rapport

1. Introduction
   1. Contexte: Machine Learning, NLP, Chatbots
   2. CNN et RNN: Explication et différences
   3. Problématique: Comment faire un chatbot?
2. How does ChatGPT works?
   1. LLM et GPT
   2. Word Encoding
   3. Training

- Attention mechanism
- Performance et limites
- Transformer: Architecture et fonctionnement, Parallelization, Self-attention
