% Corps du rapport

\section{Introduction}
\subsection{Context}
Since early 2010s, Artificial Intelligence has known an unprecedented
growth. Indeed, the progress in the field of \gls{Machine Learning} has allowed
to create algorithms capable of solving complex problems, such as image
recognition or automatic translation. These algorithms are used in many
fields, such as medicine, finance or robotics.

One of the fields where Artificial Intelligence has known the most success
is \gls{NLP}. Indeed, \gls{Machine Learning} algorithms have allowed to create
models capable of understanding human language. These models are used in
many fields, such as automatic translation, speech recognition or chatbots.

In this report, we will focus on how we can use \glspl{Transformer} to
create an automatic translation algorithm. We will first explain what 
\glspl{Transformer} are, and how they work. Then we will explain how impactful
they have been in the field of automatic translation.

\section{Transformers}
\subsection{A quick introduction to neural networks}
Before we can explain what \glspl{Transformer} are, we need to explain
what neural networks are. \glspl{Neural Networks} are a set of algorithms that are
designed to recognize patterns. They are inspired by the human brain, and
they are composed of neurons. These neurons are organized in layers, and
they are connected to each other. The first layer is called the input
layer, and the last layer is called the output layer. The layers between
the input and output layers are called hidden layers.\\

There are two broad types of neural networks: \glspl{FNN} and \glspl{RNN}. \glspl{FNN}
are the simplest type of neural networks. The information moves in only one direction,
forward from the input nodes, through the hidden nodes (if any) and to the output nodes.
There are no cycles or loops in the network \cite{schmidhuberDeepLearningNeural2015}.

% Insert image of a FNN
\begin{figure}[H]
    \centering
    \copyrightbox[b]
        {\includegraphics[width=0.5\linewidth]{images/FNN.png}}
        {Source: \href{https://www.researchgate.net/figure/The-structure-of-the-two-layered-feed-forward-neural-network_fig2_282818770}{\textsc{Training back propagation neural networks using asexual reproduction optimization}}}
    \caption{A Feedforward Neural Network}
    \label{fig:fnn}
\end{figure}

\glspl{RNN} are designed to recognize a data's sequential characteristics
and use patterns to predict the next likely scenario. The hidden layer
in \glspl{RNN} act as a memory, which allows them to remember previous
inputs. This method is called bidirectional flow. Each input of that sequence
has some relationship with its neighbors or has some influence on them.
\glspl{RNN} are used in many fields, such as handwriting recognition \cite{gravesMultidimensionalRecurrentNeural2007}, language
translation \cite{sutskeverSequenceSequenceLearning} or time series prediction \cite{petnehaziRecurrentNeuralNetworks2018}.

% Insert image of a RNN
\begin{figure}[H]
    \centering
    \copyrightbox[b]
        {\includegraphics[width=0.8\textwidth]{images/RNN-unrolled.png}}
        {Source: \href{https://colah.github.io/posts/2015-08-Understanding-LSTMs/}{\textsc{Colah's Blog}}}
    \caption{A Recurrent Neural Network}
    \label{fig:rnn}
\end{figure}

However, \glspl{RNN} have two major issues. Firstly, they are slow to train. Secondly,
they suffer from short-term memory. Indeed, they are unable to remember
information from the beginning of the sequence. This is called the vanishing
gradient problem \cite{basodiGradientAmplificationEfficient2020}.\\


To solve this problem, \glspl{LSTM} were created. \glspl{LSTM} \cite{hochreiterLongShorttermMemory1997} are a special kind of \glspl{RNN} that are capable
of learning long-term dependencies. They include a branch that allows to retain information
for later use. It improves the vanishing gradient problem but around 1000 words, it
starts to forget the beginning of the sequence. Furthermore, they are even slower to
train than \glspl{RNN}. Finally, \glspl{LSTM} take input sequentially one by one, which
makes them difficult to parallelize.

\subsection{Attention}
An Attention mechanism allows the model to focus on the most relevant parts of the input
sequence. Attention-based models are classified into two categories: \textit{global} and \textit{local} \cite{luongEffectiveApproachesAttentionbased2015a}.
Common to these two types of models is the fact that they first take as input the hidden state $h_t$
at each time step $t$. Then, they compute a context vector $c_t$ that captures relevant source-side information
for each target word. The concatenation of the hidden state $h_t$ and the context vector $c_t$ is used
to compute the attentional vector $\tilde{h}_t$. Finally, the attentional vector is used to predict the target word.
The difference between the two types of models is in the way they compute the context vector.
Two popular attention-based models are the \textit{Bahdanau} and \textit{Luong} models \cite{bahdanauNeuralMachineTranslation2016, luongEffectiveApproachesAttentionbased2015a}.
You can find a possible way to compute the output in the equations (\ref{eq:attentional_vector}) and (\ref{eq:output}) with $p(y_t | y_{<t}, x)$
being the probability of the target word $y_t$ given the previous target words $y_{<t}$ and the source sentence $x$
and $W_c$ and $W_s$ being the weight matrices of the context and softmax layers respectively that are learned during training:

\begin{equation}\label{eq:attentional_vector}
    \tilde{h}_t = \tanh(W_c[h_t; c_t])
\end{equation}
\begin{equation}\label{eq:output}
    p(y_t | y_{<t}, x) = \text{softmax}(W_s\tilde{h}_t)
\end{equation}

\subsubsection{Global Attention}
The global attention model considers all the hidden states of the source sentence to compute the context vector.
In order to do that, we create a variable-length alignment vector $a_t$, whose length is equal to the length
of the source sentence. The alignment vector is computed using the equations (\ref{eq:alignment_vector_simple}) and (\ref{eq:alignment_vector_detail}) which compares the current
hidden state $h_t$ with each source hidden state $\bar{h}_s$ \cite{luongEffectiveApproachesAttentionbased2015a}:

\begin{equation}\label{eq:alignment_vector_simple}
    a_t(s) = \text{align}(h_t, \bar{h}_s)
\end{equation}
\begin{equation}\label{eq:alignment_vector_detail}
    a_t(s) = \frac{\exp(\text{score}(h_t, \bar{h}_s))}{\sum_{s'} \exp(\text{score}(h_t, \bar{h}_{s'}))}
\end{equation}

The score function can be computed using different methods. One of the most popular methods is the dot product
between the current hidden state and the source hidden state $score(h_t, \bar{h}_s) = h_t^T\bar{h}_s$. The context vector
$c_t$ is then computed using the alignment vector $a_t$ and the source hidden states $\bar{h}_s$ \cite{luongEffectiveApproachesAttentionbased2015a}:

\begin{equation}\label{eq:context_vector}
    c_t = \sum_s a_t(s)\bar{h}_s
\end{equation}

\subsubsection{Local Attention}
The local attention model, on the other hand, only considers a subset of the source hidden states to compute the context vector.
The subset is determined by a window centered around the current target position. In concrete details, the model generates
an aligned position $p_t$ for each target position $t$. The aligned position can either be assumed to be equal to $t$ in the case of a \textit{monotonic
alignment} assuming that the target position is aligned with the source position or be predicted by the model in the case of a \textit{predictive alignment}.
The equation (\ref{eq:aligned_position}) shows how the aligned position is computed in the case of a predictive alignment with $v_p$ and $W_p$ being learned through training
and $S$ being the source sentence length \cite{luongEffectiveApproachesAttentionbased2015a}:

\begin{equation}\label{eq:aligned_position}
    p_t = S \cdot \text{sigmoid}(v_p^T\tanh(W_ph_t))
\end{equation}

The context vector $c_t$ is then computed as the weighted average over the set of source hidden states $\bar{h}_s$ within
the window $[p_t - D, p_t + D]$ with $D$ being the window size empirically selected. The alignment vector $a_t$ is computed
to favor alignment points near the aligned position $p_t$ using the equation (\ref{eq:alignment_vector_local}) with $\sigma$ being the standard deviation of the Gaussian distribution \cite{luongEffectiveApproachesAttentionbased2015a}:

\begin{equation}\label{eq:alignment_vector_local}
    a_t(s) = align(h_t, \bar{h}_s) \cdot \text{exp}(-\frac{(s - p_t)^2}{2\sigma^2})
\end{equation}

\begin{figure}
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth, height=.4\textheight,keepaspectratio]{images/local_attention_model.png}
        \caption{Global attention model}
        \label{fig:GlobalAttentionModel}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth, height=.4\textheight,keepaspectratio]{images/local_attention_model.png}
        \caption{Local attention model}
        \label{fig:LocalAttentionModel}
    \end{subfigure}
    \caption{Attention mechanism}
    \label{fig:attention}
\end{figure}

\subsection{The Transformer}
The Transformer is a novel neural network architecture that was introduced in the paper "Attention is All You Need" \cite{vaswaniAttentionAllYou2023}.
It is based on the attention mechanism and it is designed to solve the vanishing gradient problem. The goal is to reduce the number of sequential operations
and to train the model faster than \glspl{LSTM}. The main idea is to remove the use of \glspl{RNN} or \glspl{CNN} and to use only self-attention layers. In the previous
models like Conv2Seq \cite{gehringConvolutionalSequenceSequence2017} or ByteNet \cite{kalchbrennerNeuralMachineTranslation2017}, the input sequence is passed through
a stack of convolutional layers to extract local features. Then, the output of the convolutional layers is passed through a stack of \glspl{RNN} to extract global features.
The number of operations required to relate signals from two arbitrary input or output positions grows in the distance between positions, linearly for Conv2Seq and logarithmically
for ByteNet. In the Transformer, this is reduced to a constant number of operations, improving the training speed and the performance of the model.

The Transformer is composed of two main parts: the encoder and the decoder. The encoder maps an input sequence of symbol representations $(x_1, x_2, ..., x_n)$ to a sequence of continuous
representations $(z_1, z_2, ..., z_n)$. Given this vector representation, the decoder then generates an output sequence of symbols $(y_1, y_2, ..., y_m)$ one element at a time. It continuously
uses the previously generated elements to generate the next one.

\begin{figure}[H]
    \centering
    \includegraphics[width = \textwidth, height=.6\textheight,keepaspectratio]{images/transformer_architecture.png}
    \caption{The Transformer architecture}
    \label{fig:transformer_architecture}
\end{figure}

If you want to train a translator for English to French, we give the English sentence to the encoder and the French sentence to the decoder.

\subsubsection{Encoder block}
The encoder is composed of an embedding layer, a positional encoding layer and a stack of $N$ identical layers. The input sequence is first passed through the embedding layer
to convert the input tokens (words) into continuous representations. The idea is to assign to every word a unique vector representation and to keep words with similar meanings close
to each other. Then, the positional encoding layer is used to add information about the position of the word in the sequence in order to give context to the model. Once the input sequence
is embedded and positional encoded, it is passed through the Multi-Head Attention layer. For every word in the sequence, the model computes multiple attention vectors per word in parallel in
order to capture different interactions with the other words in the sequence. Then, it takes a weighted average of the attention vectors to get the final attention vector of every word.
The output of the Multi-Head Attention layer is passed through a simple position-wise fully connected feed-forward network. Since every attention vector is independent of the others, the
model can parallelize the computation of the attention vectors, unlike in \glspl{RNN} or \glspl{LSTM}.

\subsubsection{Decoder block}
The decoder is composed of an embedding layer, a positional encoding layer, a stack of $N$ identical layers and a linear layer. The Masked Multi-Head Attention layer is used to prevent
the model from looking at the future words in the sequence, which is not possible in a real-world scenario. To explain the need of such a layer, we need to understand how the model learns.
When we provide an English sentence to the encoder, it will be translated to a French sentence using only previous results. The model then compares the output with the expected French sentence
and updates the weights of the model. If the model looks at the future words in the sequence, it will be able to cheat and to generate the correct translation without learning anything. So, we
need to make the model look only at the $i$ first words in the sequence to generate the $i$-th word. Now, the output of the Masked Multi-Head Attention layer is passed through the Multi-Head
Self-Attention layer with the encoder output. The idea is to make the mapping between the English and French words and to capture the interactions between the words in the two sequences. The output
is an attention vector representing the relationship with other words in both languages. Finally, the attention vector is passed through a simple position-wise fully connected feed-forward network
and a linear layer to generate the output sequence. The choosen word is the one with the highest probability.

Now we will do a deep dive into each layer of the Transformer. In this work, $N=6$, $d_{model}=512$, $h=8$, $d_k=d_v=d_{model}/h$ \cite{vaswaniAttentionAllYou2023}.

\subsubsection{Positional encoding}
As explained earlier, the positional encoding layer is used to add information about the position of the word in the sequence. The positional encoding have the same dimension $d_{model}$ as the word embeddings
so they can be summed. There are many ways to compute the positional encoding. The authors of the paper used the formulas (\ref{eq:positional_encoding_sin}) and (\ref{eq:positional_encoding_cos}) where $pos$ is the position of the word in the sequence,
$i$ is the dimension of the positional encoding. The authors chose this method because it allows the model to easily learn to attend by relative positions, which is important for translation, since for any
offset $k$, $PE_{pos+k}$ can be represented as a linear function of $PE_{pos}$. This also allows the models to extrapolate to sequence lengths longer than the ones encountered during training.

\begin{equation}\label{eq:positional_encoding_sin}
    PE_{(pos, 2i)} = \sin\left(\frac{pos}{10000^{2i/d_{model}}}\right)
\end{equation}
\begin{equation}\label{eq:positional_encoding_cos}
    PE_{(pos, 2i+1)} = \cos\left(\frac{pos}{10000^{2i/d_{model}}}\right)
\end{equation}

\subsubsection{Multi-Head Attention}
The Multi-Head Attention layer is used to compute multiple attention vectors per word in parallel. It uses multiple layers of Scaled Dot-Product Attention to compute simultaneous attention vectors.
The Scaled Dot-Product Attention is computed using the equation (\ref{eq:attention_score}). The input is first passed through three linear layers to create the query, key and value vectors, respectively of size $d_k$, $d_k$ and $d_v$.
The query and key vectors are then multiplied together to get the attention score, each divided by $\sqrt{d_k}$ to prevent the vanishing gradient problem. The attention score is then passed through the softmax function to get the attention weights.
The queries, keys and values are in practice packed together in to matrices $Q$, $K$ and $V$.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth, height=.4\textheight,keepaspectratio]{images/scaled_dot_product.png}
        \caption{Scaled Dot-Product Attention}
        \label{fig:ScaledDotProductAttention}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth, height=.4\textheight,keepaspectratio]{images/multi_head_attention.png}
        \caption{Multi-Head Attention}
        \label{fig:MultiHeadAttention}
    \end{subfigure}
    \caption{Multi-Head Attention}
    \label{fig:multi_head_attention}
\end{figure}

Multi-Head Attention then project the queries, keys and values $h$ times with different, learned linear projections to $d_k$, $d_k$ and $d_v$ dimensions. The attention scores are then computed in parallel and concatenated.
The concatenated attention scores are then passed through a final linear layer to get the final attention vector. The Multi-Head Attention layer is computed using the equations (\ref{eq:attention_score}), (\ref{eq:attention_softmax}) and (\ref{eq:attention_output}).

\begin{equation}\label{eq:attention_score}
    \text{Attention}(Q, K, V) = \text{softmax}\left(\frac{QK^T}{\sqrt{d_k}}\right)V
\end{equation}
\begin{equation}\label{eq:attention_softmax}
    \text{softmax}(x_i) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}
\end{equation}
\begin{equation}\label{eq:attention_output}
    \text{MultiHead}(Q, K, V) = \text{Concat}(\text{head}_1, \text{head}_2, ..., \text{head}_h)W^O
\end{equation}

\subsubsection{Position-wise Feed-Forward Networks}
The Position-wise Feed-Forward Networks layer consist of two linear transformations with a ReLU activation in between (two convolutional layers with kernel size 1).
The input is first passed through the first linear layer to get the intermediate representation. Then, the intermediate representation is passed through the ReLU activation function to get the final representation.
The final representation is then passed through the second linear layer to get the output of the layer. The Position-wise Feed-Forward Networks layer is used to capture the interactions between the words in the sequence.
The inner dimension of the Position-wise Feed-Forward Networks layer is $d_{ff}=2048$.

\begin{equation}\label{eq:position_wise_feed_forward}
    \text{FFN}(x) = \max(0, xW_1 + b_1)W_2 + b_2
\end{equation}

\section{Training and Results}
The model was trained on the WMT 2014 English-French dataset of about 36M sentences and 32000 words.
The model was trained for 100,000 steps with a batch size of 25000 tokens on 8 NVIDIA P100 GPUs. Each training step took about 0.4 seconds for a total of 12 hours.
Another model named \textit{big} was trained on the same dataset for 300000 steps. Each training step took about 1.0 seconds for a total of 3.5 days.

The results of the model were compared to the results of the previous state-of-the-art models. The model achieved a BLEU score of 41.8 on the newstest2014 dataset, which is an improvement of 2.2 BLEU points over the previous state-of-the-art model for a reduced training cost \cite{vaswaniAttentionAllYou2023}.

\begin{figure}[H]
    \centering
    \includegraphics[width = \textwidth, height=.6\textheight,keepaspectratio]{images/english_to_french.png}
    \caption{Results of the Transformer model}
    \label{fig:transformer_results}
\end{figure}

\section{Conclusion}
In this report, we have explained what \glspl{Transformer} are, and how they work. We have also explained how impactful they have been in the field of automatic translation. We have seen that the \glspl{Transformer} model
achieved a BLEU score of 41.8 on the newstest2014 dataset, which is an improvement of 2.2 BLEU points over the previous state-of-the-art model for a reduced training cost. We have also seen that the \glspl{Transformer} model
is faster to train than \glspl{LSTM} and \glspl{RNN}. The Transformer model is now widely used in the field of automatic translation, and it has allowed to create more accurate and faster automatic translation algorithms but
also in the field of chatbots and speech recognition. Soon, it may be widely used in the field of medicine, finance or robotics.

For additional information, you can check Jay Alammar's blog amazing Github repository \href{https://jalammar.github.io/visualizing-neural-machine-translation-mechanics-of-seq2seq-models-with-attention/}{Visualizing A Neural Machine Translation Model (Mechanics of Seq2seq Models With Attention)}.
It contains a lot of visualizations and explanations about the Transformer model and the attention mechanism.

% Plan du rapport:
% 1. Introduction
    % 1. Contexte: Machine Learning, NLP, Chatbots
    % 2. Problèmatique: Comment utiliser les CNN et RNN pour créer un chatbot?

% 2. Les Transformers

% 2 broad types of neural networks: Feedforward neural network and Recurrent
% neural network.

% Feedforward neural network: The information moves in only one direction,
% forward from the input nodes, through the hidden nodes (if any) and to the
% output nodes. There are no cycles or loops in the network.

% Recurrent neural network: The connections between the nodes form a directed
% graph along a sequence. This allows it to exhibit dynamic temporal behavior
% for a time sequence. Unlike feedforward neural networks, RNNs can use their
% internal state (memory) to process sequences of inputs.

% RNN Major issue: Short-Term Memory. It's inability to remember information

% Long Short-Term Memory: Slow to train

% Attention
% see video : https://jalammar.github.io/images/seq2seq_7.mp4
% see video : https://jalammar.github.io/images/attention_process.mp4
% see video : https://jalammar.github.io/images/attention_tensor_dance.mp4
% Source : https://arxiv.org/abs/1409.0473
% Source : https://arxiv.org/abs/1508.04025

% Transformer
% Source : https://arxiv.org/abs/1706.03762
% Architecture based on attention layers
% Input sequence can be passed parallelly
% Multi-headed attention layer
% Solve vanishing gradient problem and train faster than LSTM

% Encoder block

% Decoder block

% Application & Results
% Source : https://blog.research.google/2017/08/transformer-novel-neural-network.html
% Video : https://3.bp.blogspot.com/-aZ3zvPiCoXM/WaiKQO7KRnI/AAAAAAAAB_8/7a1CYjp40nUg4lKpW7covGZJQAySxlg8QCLcBGAs/s640/transform20fps.gif
% BERT Model : https://builtin.com/data-science/using-bert-battle-job-scams
% Results : English to German translation
% Source : https://arxiv.org/abs/1706.03762

