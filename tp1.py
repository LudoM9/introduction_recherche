import numpy
from numpy.linalg import inv
import cvxopt
import picos as pic

# System parameters
A_mat = numpy.matrix([
    [0.0, 1.0],
    [-1.0, 2.0]
])
# Create problem instance
prob = pic.Problem()
# Define variables
t = pic.RealVariable("t", 1)
P = pic.SymmetricVariable("P", 2)
# Define parameters
A = pic.Constant("A", A_mat)
I = pic.Constant("I", numpy.identity(2))
# Constraints
prob.add_constraint(P >> 0)
prob.add_constraint(t >> 0)
prob.add_constraint(A.T * P + P * A - t*I << 0)
# Objective
prob.set_objective("min", t)
prob.solve()

print(P)
