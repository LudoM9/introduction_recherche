# available at https://www.ensta-bretagne.fr/jaulin/roblib.py
# For help : https://www.ensta-bretagne.fr/jaulin/python.html
# used in KalMOOC :  https://www.ensta-bretagne.fr/kalmooc/
# used in RobMOOC :  https://www.ensta-bretagne.fr/robmooc/
# used in KalMOOC :  https://www.ensta-bretagne.fr/inmooc/

from typing import Any, Tuple

import numpy as np
from matplotlib.cbook import flatten
from matplotlib.collections import PatchCollection
from matplotlib.patches import Ellipse, Polygon, Rectangle
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D
from numpy import (arange, arccos, arcsin, arctan2, array, cos, diag, eye,
                   hstack, linspace, log, mean, meshgrid, ones, pi, sign, sin,
                   size, sqrt, sum, vstack, zeros)
from numpy.linalg import eig, inv, norm
from numpy.random import rand, randn
from scipy.linalg import expm, logm, norm, sqrtm
from scipy.signal import place_poles

def scalarprod(u : np.ndarray, v : np.ndarray) -> float:
    """Scalar product between two vectors

    Args:
        u (np.ndarray): First vector
        v (np.ndarray): Second vector

    Returns:
        float: Scalar product of vectors u and v
        
    Examples:
        >>> scalarprod(array([[1], [2]]), array([[3], [4]]))
        11
    """
    u, v = u.flatten(), v.flatten()
    return sum(u[:] * v[:])

def angles_to_eulermat(phi : float, theta : float, psi : float) -> np.ndarray:
    """Computes the euler matrix associated to the 3 euler angles

    Args:
        phi (float): Yaw
        theta (float): Pitch
        psi (float): Roll

    Returns:
        np.ndarray: Euler Matrix
        
    Examples:
        >>> angles_to_eulermat(0, 0, pi/2)
        array([[ 0. , -1. ,  0.],
               [ 1. ,  0. ,  0.],
               [ 0. ,  0. ,  1.]])
    """
    return expw([0, 0, psi]) @ expw([0, theta, 0]) @ expw([phi, 0, 0])

def eulermat_to_angles(R : np.ndarray) -> tuple:
    """Returns the 3 euler angles associated to the euler matrix

    Args:
        R (np.ndarray): Euler Matrix

    Returns:
        tuple: Float corresponding to phi, theta and psi
        
    Examples:
        >>> eulermat_to_angles(array([[ 0. , -1. ,  0.],
                                      [ 1. ,  0. ,  0.],
                                      [ 0. ,  0. ,  1.]]))
        (0.0, 0.0, 1.5707963267948966)
    """
    phi = arctan2(R[2, 1], R[2, 2])
    theta = -arcsin(R[2, 0])
    psi = arctan2(R[1, 0], R[0, 0])
    return phi, theta, psi

def eulerderivative(phi : float, theta : float) -> np.ndarray:
    """Returns the derivative of the Euler matrix associated to the 2 euler angles

    Args:
        phi (float): Yaw
        theta (float): Pitch

    Returns:
        np.ndarray: Derivative of Euler Matrix
    
    Examples:
        >>> eulerderivative(pi/2, -pi)
        array([[ 1.00000000e+00 , -1.22464680e-16 , -7.49879891e-33],
               [ 0.00000000e+00 ,  6.12323400e-17 , -1.00000000e+00],
               [ 0.00000000e+00 , -1.00000000e+00 , -6.12323400e-17]])
    """
    cphi = cos(phi)
    sphi = sin(phi)
    ctheta = cos(theta)
    ttheta = sin(theta) / cos(theta)
    return array([[1, sphi * ttheta, cphi * ttheta],
                  [0, cphi         ,-sphi         ],
                  [0, sphi / ctheta, cphi / ctheta]])

def angle(x : np.ndarray) -> float:
    """Compute the angle of a vector represented by a 2D Numpy Array

    Args:
        x (np.ndarray): Vector

    Returns:
        float: Angle of the vector
    
    Examples:
        >>> angle(array([[1], [2]]))
        1.1071487177940904
    """
    x = x.flatten()
    return arctan2(x[1], x[0])

def add1(M : np.ndarray) -> np.ndarray:
    """Adds to the array M a line of 1 at the bottom and returns it

    Args:
        M (np.ndarray): Numpy Array to be transformed

    Returns:
        np.ndarray: M array with one more line of 1 at the bottom
    
    Examples:
        >>> add1(array([[1, 2, 3], [4, 5, 6]]))
        array([[1, 2, 3],
               [4, 5, 6],
               [1, 1, 1]])
    """
    M = array(M)
    return vstack((M, ones(M.shape[1])))

def tolist(w : np.ndarray) -> list:
    """Transform an array into a list beginning with the first line

    Args:
        w (np.ndarray): Numpy Array to be transformed

    Returns:
        list: List of all elements inside the array
    
    Examples:
        >>> tolist(array([[1, 2, 3], [4, 5, 6]]))
        [1, 2, 3, 4, 5, 6]
    """
    return list(flatten(w))

def adjoint(w : np.ndarray) -> np.ndarray:
    """Computes the adjoint of w. Also works if w is a scalar

    Args:
        w (np.ndarray): Vector from which we want to compute the adjoint.

    Returns:
        np.ndarray: Adjoint of w
    
    Examples:
        >>> adjoint(array([[1], [2], [3]]))
        [[ 0 , -3 ,  2],
         [ 3 ,  0 , -1],
         [-2 ,  1 ,  0]]
    """
    if isinstance(w, (float, int)):
        return array([[0, -w], [w, 0]])
    w = tolist(w)
    return array([[0, -w[2], w[1]], [w[2], 0, -w[0]], [-w[1], w[0], 0]])

def adjoint_inv(A : np.ndarray) -> np.ndarray:
    """Computes the inverse of the adjoint.
    It can return either a scalar if A is 2x2 Numpy Array or a vector if A is a 3x3.

    Args:
        A (np.ndarray): Adjoint

    Returns:
        np.ndarray: Inverse of the adjoint
    
    Examples:
        >>> adjoint_inv(array([[0, -3, 2], [3, 0, -1], [-2, 1, 0]]))
        array([[1], [2], [3]])
    """
    if size(A) == 4:
        return A[1, 0]  # A is 2x2
    return array([[A[2, 1]], [A[0, 2]], [A[1, 0]]])  # A is 3x3

def ToH(R : np.ndarray) -> np.ndarray:
    """Transforms a rotation matrix into a homogeneous matrix

    Args:
        R (np.ndarray): Rotation Matrix

    Returns:
        np.ndarray: Homogeneous Matrix
        
    Examples:
        >>> ToH(array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
        array([[1, 2, 3, 0],
               [4, 5, 6, 0],
               [7, 8, 9, 0],
               [0, 0, 0, 1]])
    """
    H = hstack((R, array([[0], [0], [0]])))
    V = vstack((H, array([0, 0, 0, 1])))
    return V

def expw(w : np.ndarray) -> np.ndarray:
    """Computes the exponential of w vector

    Args:
        w (np.ndarray): Vector

    Returns:
        np.ndarray: Exponential of w matrix
    
    Examples:
        >>> expw(array([[1], [2], [3]]))
        array([[-0.69492056 ,  0.71352099 , 0.08929286],
               [-0.19200697 , -0.30378504 , 0.93319235],
               [ 0.69297817 ,  0.6313497  , 0.34810748]])
    """
    return expm(adjoint(w))

def expwH(w : np.ndarray) -> np.ndarray:
    """Computes the exponential of w matrix and transforms it into a homogeneous matrix

    Args:
        w (np.ndarray): Matrix

    Returns:
        np.ndarray: Homogeneous exponential of w matrix
    
    Examples:
        >>> expwH(array([[1], [2], [3]]))
        array([[-0.69492056 ,  0.71352099 , 0.08929286 , 0.        ],
               [-0.19200697 , -0.30378504 , 0.93319235 , 0.        ],
               [ 0.69297817 ,  0.6313497  , 0.34810748 , 0.        ],
               [ 0.         ,  0.         , 0.         , 1.        ]])
    """
    return ToH(expw(w))

def logw(R : np.ndarray) -> np.ndarray:
    """Computes the logarithm of R matrix

    Args:
        R (np.ndarray): Matrix

    Returns:
        np.ndarray: Logarithm of R matrix
    
    Examples:
        >>> logw(array([[-0.69492056 ,  0.71352099 , 0.08929286],
                        [-0.19200697 , -0.30378504 , 0.93319235],
                        [ 0.69297817 ,  0.6313497  , 0.34810748]]))
        array([[-0.67925192],
               [-1.35850381],
               [-2.03775572]])
    """
    return adjoint_inv(logm(R))

def Rlatlong(lx : float, ly : float) -> np.ndarray:
    """Computes the rotation matrix associated to the latitude and longitude.
    
    Args:
        lx (float): Latitude
        ly (float): Longitude
    
    Returns:
        np.ndarray: Rotation Matrix
    
    Examples:
        >>> Rlatlong(48.418859, -4.473766)
        array([[ 0.96220026,  0.26462589,  0.06437229],
               [-0.2723429 ,  0.93493572,  0.22743032],
               [ 0.        , -0.23636485,  0.97166438]])
    """
    return angles_to_eulermat(0, 0, lx) @ angles_to_eulermat(0, -pi / 2 + ly, -pi / 2).T

def tran2H(x : float, y : float) -> np.ndarray:
    """2D homogenous transformation matrix for a translation of (x, y).

    Args:
        x (float): x coordinate
        y (float): y coordinate

    Returns:
        np.ndarray: Homogenous transformation matrix
    
    Examples:
        >>> tran2H(1, 2)
        array([[1, 0, 1],
               [0, 1, 2],
               [0, 0, 1]])
    """
    return array([[1, 0, x], [0, 1, y], [0, 0, 1]])

def rot2w(R : np.ndarray) -> np.ndarray:
    """Transforms a rotation matrix into a rotation vector.

    Args:
        R (np.ndarray): Rotation Matrix

    Returns:
        np.ndarray: Rotation Vector
    
    Examples:
        >>> M = Rlatlong(48.418859, -4.473766)
        >>> R = rot2w(M)
        array([[-0.23710531],
               [ 0.03290895],
               [-0.27451375]])
    """
    return adjoint_inv(logm(R))

def rot2H(a : float) -> np.ndarray:
    """2D homogenous transformation matrix for a rotation of a radians.

    Args:
        a (float): Angle of rotation in radian

    Returns:
        np.ndarray: Homogenous transformation matrix
    
    Examples:
        >>> rot2H(pi/2)
        array([[ 6.123234e-17, -1.000000e+00,  0.000000e+00],
               [ 1.000000e+00,  6.123234e-17,  0.000000e+00],
               [ 0.000000e+00,  0.000000e+00,  1.000000e+00]])
    """
    a = float(a)
    return array([[cos(a), -sin(a), 0], [sin(a), cos(a), 0], [0, 0, 1]])

def eulerH(phi : float, theta : float, psi : float) -> np.ndarray:
    """3D homogenous transformation matrix for a rotation of phi, theta and psi radians.

    Args:
        phi (float): Yaw
        theta (float): Pitch
        psi (float): Roll

    Returns:
        np.ndarray: Homogenous transformation matrix
    
    Examples:
        >>> eulerH(0, 0, pi/2)
        array([[ 0. , -1. , 0. , 0.],
               [ 1. ,  0. , 0. , 0.],
               [ 0. ,  0. , 1. , 0.],
               [ 0. ,  0. , 0. , 1.]])
    """
    return ToH(expw([0, 0, psi]) @ expw([0, theta, 0]) @ expw([phi, 0, 0]))

def tran3H(x : float, y : float, z : float) -> np.ndarray:
    """3D homogenous transformation matrix for a translation of (x, y, z).

    Args:
        x (float): x coordinate
        y (float): y coordinate
        z (float): z coordinate

    Returns:
        np.ndarray: Homogenous transformation matrix
    
    Examples:
        >>> tran3H(1, 2, 3)
        array([[1, 0, 0, 1],
               [0, 1, 0, 2],
               [0, 0, 1, 3],
               [0, 0, 0, 1]])
    """
    return array([[1, 0, 0, x], [0, 1, 0, y], [0, 0, 1, z], [0, 0, 0, 1]])

def rot3H(wx : float, wy : float, wz : float) -> np.ndarray:
    """3D homogenous transformation matrix for a rotation of wx, wy and wz radians.
    
    Args:
        wx (float): Rotation around x axis
        wy (float): Rotation around y axis
        wz (float): Rotation around z axis
        
    Returns:
        np.ndarray: Homogenous transformation matrix
        
    Examples:
        >>> rot3H(pi/2, 0, 0)
        array([[ 1. , 0. , 0. , 0.],
               [ 0. , 0. ,-1. , 0.],
               [ 0. , 1. , 0. , 0.],
               [ 0. , 0. , 0. , 1.]])
        
    """
    return ToH(expw([wx, wy, wz]))

def angle3d(u : np.ndarray, v : np.ndarray) -> np.ndarray:
    """Returns the rotation vector between two vectors u and v with the minimal angle.
    It is the normal vector of the plane containing u and v with norm the angle between u and v.

    Args:
        u (np.ndarray): First vector
        v (np.ndarray): Second vector

    Returns:
        np.ndarray: Rotation vector between u and v
    
    Examples:
        >>> angle3d(array([[1], [2], [3]]), array([[4], [5], [6]]))
        array([[-0.09215231],
               [ 0.18430461],
               [-0.09215231]])
    """
    if norm(u) * norm(v) < 0.000000001: return 0 * u
    u = (1 / norm(u)) * array(u)
    v = (1 / norm(v)) * array(v)
    c = scalarprod(u, v)
    w = adjoint(u) @ v
    if c > 0.99: return w
    if c < -0.99: return angle3d(u, v + 0.01 * randn(3, 1))
    return (arccos(c) / norm(w)) * w

def projSO3(M : np.ndarray) -> np.ndarray:
    """Return the closest rotation matrix to M.

    Args:
        M (np.ndarray): Matrix

    Returns:
        np.ndarray: Closest rotation matrix to M
    
    Examples:
        >>> projSO3(array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
        array([[ 0.12309149 ,  0.90453403 , -0.40824829],
               [ 0.49236596 ,  0.30151134 ,  0.81649658],
               [ 0.86164044 , -0.30151134 , -0.40824829]])
    """
    Q, R = np.linalg.qr(M)
    return Q @ diag(diag(sign(R)))

def latlong2cart(rho : float, lx : float, ly : float) -> np.ndarray:
    """Computes cartesian coordinates from latitude and longitude

    Args:
        rho (float): Radius of the sphere
        lx (float): Latitude
        ly (float): Longitude

    Returns:
        np.ndarray: Vertical Numpy Array with x, y and z
        
    Examples:
        >>> latlong2cart(6371000, 48.418859, -4.473766)
        array([[410115.84441947 ],
               [1448958.5752553 ],
               [6190473.73319282]])
    """
    return rho * array([[cos(ly) * cos(lx)], [cos(ly) * sin(lx)], [sin(ly)]])

def cart2latlong(x : float, y : float, z : float) -> Tuple[float, float, float]:
    """Computes latitude, longitude and radius from cartesian coordinates
    
    Args:
        x (float): x coordinate
        y (float): y coordinate
        z (float): z coordinate

    Returns:
        tuple: radius of the sphere, latitude and longitude
    
    Examples:
        >>> cart2latlong(410115.84441947, 1448958.5752553, 6190473.73319282)
        (6371000.0, 1.2949691961530991, 1.332173346410207)
    """
    r = norm(array([x, y, z]))
    ly = arcsin(z / r)
    lx = arctan2(y, x)
    return r, lx, ly

def sawtooth(x : float) -> float:
    """Computes the sawtooth function
    
    Args:
        x (float): Angle to compute
    
    Returns:
        float: Sawtooth function of x
    
    Examples:
        >>> sawtooth(3*pi + pi/2)
        -1.5707963267948966
    """
    return (x + pi) % (2 * pi) - pi  # or equivalently   2*arctan(tan(x/2))

def place(A : np.ndarray, B : np.ndarray, poles : np.ndarray) -> np.ndarray:
    """Computes the gain matrix K such that the poles of the system (A-BK) are the ones given in poles

    Args:
        A (np.ndarray): State-space representation of linear system ``AX + BU``
        B (np.ndarray): State-space representation of linear system ``AX + BU``
        poles (np.ndarray): Desired poles

    Returns:
        np.ndarray: Gain matrix K
    
    Examples:
        >>> A = np.array([[ 0,  7/3.,  0,   0   ],
                          [ 0,   0,    0,  7/9. ],
                          [ 0,   0,    0,   0   ],
                          [ 0,   0,    0,   0   ]])
        >>> B = np.array([[ 0,  0 ],
                          [ 0,  0 ],
                          [ 1,  0 ],
                          [ 0,  1 ]])
        >>> P = np.array([-3, -1, -2-1j, -2+1j]) / 3.
        >>> K = place(A, B, P)
        array([[-0.18123374 , -0.00789724 ,  0.71602593 , -0.12692124],
               [ 0.15052711 ,  1.63797106 , -0.03167454 ,  1.95064074]])
    """
    return place_poles(A, B, poles).gain_matrix

######## DRAWING ########

def init_figure(xmin=-10.0, xmax=10.0, ymin=-10.0, ymax=10.0) -> Axes:
    """Initialize a figure with axis between the registered values.

    Args:
        xmin (float, optional): Minimum value of x axis. Defaults to -10.0.
        xmax (float, optional): Maximum value of x axis. Defaults to 10.0.
        ymin (float, optional): Minimum value of y axis. Defaults to -10.0.
        ymax (float, optional): Maximum value of y axis. Defaults to 10.0.

    Returns:
        Axes: Figure
    
    Examples:
        >>> init_figure(-10, 10, -10, 10)
    Or simply:
        >>> init_figure()
    """
    fig = figure()
    ax = fig.add_subplot(111, aspect='equal')
    ax.xmin = xmin
    ax.xmax = xmax
    ax.ymin = ymin
    ax.ymax = ymax
    clear(ax)
    return ax

def clear(ax : Axes) -> None:
    """Clear the figure of all drawings

    Args:
        ax (Axes): Figure
    
    Examples:
        >>> clear(ax)
    """
    pause(0.001)
    cla()
    ax.set_xlim(ax.xmin, ax.xmax)
    ax.set_ylim(ax.ymin, ax.ymax)

def axis3D(x1=-10.0, x2=10.0, y1=-10.0, y2=10.0, z1=-10.0, z2=10.0) -> Axes3D:
    """Initialize a 3D figure with axis between the registered values.

    Args:
        x1 (float, optional): Minimum value of x axis. Defaults to -10.0.
        x2 (float, optional): Maximum value of x axis. Defaults to 10.0.
        y1 (float, optional): Minimum value of y axis. Defaults to -10.0.
        y2 (float, optional): Maximum value of y axis. Defaults to 10.0.
        z1 (float, optional): Minimum value of z axis. Defaults to -10.0.
        z2 (float, optional): Maximum value of z axis. Defaults to 10.0.

    Returns:
        Axes3D: 3D Figure
    
    Examples:
        >>> axis3D(-10, 10, -10, 10, -10, 10)
    Or simply:
        >>> axis3D()
    """
    fig = figure()
    ax = subplot(111, projection="3d")
    ax.set_xlim3d(x1, x2)
    ax.set_ylim3d(y1, y2)
    ax.set_zlim3d(z1, z2)
    fig.add_axes(ax)
    return ax

def clean3D(ax : Axes3D) -> None:
    """Clear the 3D figure of all drawings

    Args:
        ax (Axes3D): Figure
    
    Examples:
        >>> clean3D(ax)
    """
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    z1, z2 = ax.get_zlim()
    ax.clear()
    ax.set_xlim3d(x1, x2)
    ax.set_ylim3d(y1, y2)
    ax.set_zlim3d(z1, z2)

def draw_arrow3D(ax : Axes3D, x : float, y : float, z : float, wx : float, wy : float, wz : float, col : str) -> None:
    """Method that draws an arrow with initial point x and final point x+w

    Args:
        ax (Axes3D): Figure
        x (float): x coordinate of the beginning of the arrow
        y (float): y coordinate of the beginning of the arrow
        z (float): z coordinate of the beginning of the arrow
        wx (float): Value to add to x coordinates for the position of the head of the arrow
        wy (float): Value to add to y coordinates for the position of the head of the arrow
        wz (float): Value to add to z coordinates for the position of the head of the arrow
        col (str): Color of the arrow
    
    Examples:
        >>> draw_arrow3D(ax, 0, 0, 0, 1, 1, 1, "red")
    """
    ax.quiver(x, y, z, wx, wy, wz, color=col, lw=1, pivot='tail', length=norm([wx, wy, wz]), normalize=True)

def draw_axis3D(ax : Axes3D, x=0.0, y=0.0, z=0.0, R=eye(3), zoom=1) -> None:
    """Method that draws 3 axis at position x,y,z with oriention matrix R

    Args:
        ax (Axes3D): Figure
        x (float, optional): x coordinate. Defaults to 0.0.
        y (float, optional): y coordinate. Defaults to 0.0.
        z (float, optional): z coordinate. Defaults to 0.0.
        R (_type_, optional): Orientation Matrix. Defaults to eye(3).
        zoom (int, optional): Size of the arrows. Defaults to 1.
        
    Examples:
        >>> R = rot3H(0, 0, 0)
        >>> draw_axis3D(ax, 0, 0, 0, R)
    """
    ax.scatter(x, y, z, color='magenta')
    R = zoom * R
    draw_arrow3D(ax, x, y, z, R[0, 0], R[1, 0], R[2, 0], "red")
    draw_arrow3D(ax, x, y, z, R[0, 1], R[1, 1], R[2, 1], "green")
    draw_arrow3D(ax, x, y, z, R[0, 2], R[1, 2], R[2, 2], "blue")

def cube3H() -> np.ndarray:
    """Array allowing to easily draw a cube. Set this as parameter inside draw3H.
    It can also be multiplied by any scalar.

    Returns:
        np.ndarray: Cube set of points
    
    Examples:
        >>> cube3H()
    """
    return array([[0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1],
                  [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0],
                  [0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0],
                  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])

def auv3H() -> np.ndarray:
    """Array allowing to easily draw an auv3D. Set this as parameter inside draw3H.
    It can also be multiplied by any scalar.

    Returns:
        np.ndarray: Auv set of points
    
    Examples:
        >>> auv3H()
    """
    return add1(array([[0.0, 0.0, 10.0, 0.0, 0.0, 10.0, 0.0, 0.0],
                       [-1.0, 1.0, 0.0, -1.0, -0.2, 0.0, 0.2, 1.0],
                       [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0]]))

def wheel3H(r : float, n=12) -> np.ndarray:
    """Array to easily draw a wheel with n bars. Set this as parameter inside draw3H.
    It can also be multiplied by any scalar.

    Args:
        r (float): Radius of the wheel
        n (int, optional): Number of bars. Defaults to 12.

    Returns:
        np.ndarray: Wheel set of points
    
    Examples:
        >>> wheel3H(1, 12)
    """
    W0 = [[0, 0, 0], [r, 0, r], [0, 0, 0], [1, 1, 1]]
    W = W0
    R = rot3H(2 * pi / n, 0, 0)
    for i in range(n + 1):
        W0 = R @ W0
        W = hstack((W, W0))
    return W

def circle3H(r : float, n=20) -> np.ndarray:
    """Array to easily draw a circle with n segments. Set this as parameter inside draw3H.
    It can also be multiplied by any scalar.

    Args:
        r (float): Radius of the circle
        n (int, optional): Number of segments (the higher, the closer we are from a true circle).
        Defaults to 20.

    Returns:
        np.ndarray: Circle set of points
    
    Examples:
        >>> circle3H(1, 20)
    """
    theta = linspace(0, 2 * pi, n)
    x = r * cos(theta) + array(n * [0])
    y = r * sin(theta) + array(n * [0])
    z = zeros(n)
    return add1(array([x, y, z]))

def earth3H(rho : float, a=pi/10) -> np.ndarray:
    """Array to easily draw an ellipsoid with segments every a rad. Set this as parameter inside draw3H.
    It can also be multiplied by any scalar.

    Args:
        rho (float): Main radius of the ellipsoid
        a (_type_, optional): Discretisation of the angle. Defaults to pi/10.

    Returns:
        np.ndarray: Ellipsoid set of points
    
    Examples:
        >>> earth3H(10, pi/10)
    """
    Lx = arange(0, 2 * pi + a, a)
    Ly = arange(-pi / 2, pi / 2 + a, a)
    M1 = rho * array([[cos(-pi / 2) * cos(0)], [cos(-pi / 2) * sin(0)], [sin(-pi / 2)]])
    M2 = M1
    for ly1 in Ly:
        for lx1 in Lx:
            M1 = hstack((M1, rho * array([[cos(ly1) * cos(lx1)],
                                          [cos(ly1) * sin(lx1)],
                                          [sin(ly1)]])))
    for lx1 in Lx:
        for ly1 in Ly:
            M2 = hstack((M2, rho * array([[cos(ly1) * cos(lx1)],
                                          [cos(ly1) * sin(lx1)],
                                          [sin(ly1)]])))
    M = hstack((M1, M2))
    return add1(M)

def cylinder3H(r : float, L : float, n=25) -> np.ndarray:
    """Array to easily draw a cylinder with a radius r and a length L with n segments.
    Set this as parameter inside draw3H. It can also be multiplied by any scalar.

    Args:
        r (float): Radius of the cylinder
        L (float): Length of the cylinder
        n (int, optional): Number of segments (the higher, the closer we are from a true circle). Defaults to 25.

    Returns:
        np.ndarray: Cylinder set of points
    
    Examples:
        >>> cylinder3H(1, 10, 25)
    """
    W = [[0.3, 0], [0, 0], [0, 0]]
    for i in range(n + 1):
        P1 = [[0], [r * cos(2 * pi * i / n)], [r * sin(2 * pi * i / n)]]
        P2 = [[L], [r * cos(2 * pi * i / n)], [r * sin(2 * pi * i / n)]]
        W = hstack((W, P1, P2, P1, [[0], [0], [0]]))
    return add1(array(W))

def draw3H(ax : Axes3D, M : np.ndarray, col : str, shadow=False, mirror=1) -> None:
    """Draws line between n points on figure.
    M is an array with 3 lines (x,y and z coordinates) and n column.

    Args:
        ax (Axes3D): Figure
        M (np.ndarray): Numpy Array containing all the points
        col (str): Color of the lines
        shadow (bool, optional): Adds shadow on the ground to figure. Defaults to False.
        mirror (int, optional): If z is inverted, set mirror to -1. Defaults to 1.
    
    Examples:
        >>> auv = auv3H()
        >>> draw3H(ax, auv, "blue", False, 1)
    """
    if shadow: 
        ax.plot(mirror * M[0], M[1], 0 * M[2], color='gray')
    ax.plot(mirror * M[0], M[1], mirror * M[2], color=col)

def draw_earth3D(ax : Axes3D, r : float, R : np.ndarray, col='gray') -> None:
    """Draw on ax Figure an ellipsoid with radius r and orientation determined by euler matrix R.
    You can also set its color with parameter col.
    It is easy to use combined with angles_to_eulermat.

    Args:
        ax (Axes3D): Figure
        r (float): Main radius of the ellipsoid
        R (np.ndarray): Euler Matrix with the orientation of the ellipsoid
        col (str, optional): Color of the ellipsoid. Defaults to 'gray'.
    
    Examples:
        >>> R = angles_to_eulermat(0, pi/2, 0)
        >>> draw_earth3D(ax, 10, R)
    """
    plot3D(ax, ToH(R) @ earth3H(r), col)
    ax.scatter(*(R @ array([[r], [0], [0]])), color='red')

def draw_wheel3D(ax : Axes3D, x : float, y : float, z : float, phi : float, theta : float, psi : float, r=1, col='blue') -> None:
    """Draw on ax Figure a wheel with central position (x,y,z), orientation phi, theta and psi and radius r.
    You can also set its color with parameter col.

    Args:
        ax (Axes3D): Figure
        x (float): x coordinates of the center of the wheel
        y (float): y coordinates of the center of the wheel
        z (float): z coordinates of the center of the wheel
        phi (float): phi orientation of the wheel
        theta (float): theta orientation of the wheel
        psi (float): psi orientation of the wheel
        r (int, optional): radius of the wheel. Defaults to 1.
        col (str, optional): color of the wheel. Defaults to 'blue'.
    
    Examples:
        >>> draw_wheel3D(ax, 0, 0, 0, 0, pi/2, 0, r=3, col='red')
    """
    M = tran3H(x, y, z) @ eulerH(phi, theta, psi) @ wheel3H(r)
    draw3H(ax, M, col, True, 1)
    p = array([[x], [y], [z]]) + angles_to_eulermat(phi, theta, psi) @ array([[0], [1], [0]])
    ax.scatter(*p, color='red')

def draw_robot3D(ax : Axes3D, p : np.ndarray, R : np.ndarray, col='blue', size=1) -> None:
    """Draw on ax Figure an auv3D at position p (vertical Numpy Array) and Euler matrix R.
    You can also set its color with parameter col and its size with parameters size.
    
    Args:
        ax (Axes3D): Figure
        p (np.ndarray): Vertical Numpy array with the position of the auv
        R (np.ndarray): Euler Matrix with the orientation of the auv
        col (str, optional): Color of the auv. Defaults to 'blue'.
        size (int, optional): Scalar size of the auv. Defaults to 1.
    
    Examples:
        >>> R = angles_to_eulermat(0, pi/2, 0)
        >>> p = array([[0], [0], [0]])
        >>> draw_robot3D(ax, p, R, col='red')
    """
    M = tran3H(*p[0:3, 0]) @ diag([size, size, size, 1]) @ ToH(R) @ auv3H()
    draw3H(ax, M, col, True, 1)
    pause(0.001)

def draw_auv3D(ax : Axes3D, x : float, y : float, z : float, phi : float, theta : float, psi : float, col='blue', size=1) -> None:
    """Draw on ax Figure an auv3D at with position (x,y,z) and orientation phi, theta and psi.
    You can also set its color with parameter col and its size with parameters size.

    Args:
        ax (Axes3D): Figure
        x (float): x coordinates of the auv
        y (float): y coordinates of the auv
        z (float): z coordinates of the auv
        phi (float): phi orientation
        theta (float): theta orientation
        psi (float): psi orientation
        col (str, optional): Color of the auv. Defaults to 'blue'.
        size (int, optional): Scalar size of the auv. Defaults to 1.
    
    Examples:
        >>> draw_auv3D(ax, 0, 0, 0, 0, pi/2, 0, col='red')
    """
    draw_robot3D(ax, array([[x], [y], [z]]), angles_to_eulermat(phi, theta, psi), col, size)

def draw_quadrotor3D(ax : Axes3D, p : np.ndarray, R : np.ndarray, alpha : np.ndarray, l : float) -> None:
    """Draw on ax Figure a quadrotor at position p (vertical Numpy Array) and Euler matrix R.
    You can also set its size with parameters l and alpha the angle of the blades.

    Args:
        ax (Axes3D): Figure
        p (np.ndarray): Vertical Numpy array with the position of the quadrotor
        R (np.ndarray): Euler Matrix with the orientation of the quadrotor
        alpha (np.ndarray): Angle of the blades
        l (float): Size of the quadrotor
    
    Examples:
        >>> R = angles_to_eulermat(0, pi/2, 0)
        >>> p = array([[0], [0], [0]])
        >>> alpha = array([[0.1],[0.3],[0.5],[0.7]])
        >>> draw_quadrotor3D(ax, p, R, alpha, 2)
    """
    Ca = hstack((circle3H(0.3 * l), [[0.3 * l, -0.3 * l], [0, 0], [0, 0], [1, 1]]))  # the disc + the blades
    T = tran3H(p[0, 0], p[1, 0], p[2, 0]) @ ToH(R)  # I replaced tran3H(*) to avoid warning
    C0 = T @ tran3H(0, l, 0) @ eulerH(0, 0, alpha[0]) @ Ca  # we rotate the blades
    C1 = T @ tran3H(-l, 0, 0) @ eulerH(0, 0, -alpha[1]) @ Ca
    C2 = T @ tran3H(0, -l, 0) @ eulerH(0, 0, alpha[2]) @ Ca
    C3 = T @ tran3H(l, 0, 0) @ eulerH(0, 0, -alpha[3]) @ Ca
    M = T @ add1(array([[l, -l, 0, 0, 0], [0, 0, 0, l, -l], [0, 0, 0, 0, 0]]))
    draw3H(ax, M, 'grey', True, -1)  # body
    draw3H(ax, C0, 'green', True, -1)
    draw3H(ax, C1, 'black', True, -1)
    draw3H(ax, C2, 'red', True, -1)
    draw3H(ax, C3, 'blue', True, -1)

def draw_riptide3D(ax : Axes3D, p : np.ndarray, R : np.ndarray, u : np.ndarray, alpha : float) -> None:
    """Draw on ax Figure a riptide at position p (vertical Numpy Array) and Euler matrix R.
    You can also set the angles of the rudders with u and the angle of the blade with alpha.

    Args:
        ax (Axes3D): Figure
        p (np.ndarray): Vertical Numpy array with the position of the quadrotor
        R (np.ndarray): Euler Matrix with the orientation of the quadrotor
        u (np.ndarray): Matrix with the angles of the rudders
        alpha (float): Angle of the blade
    
    Examples:
        >>> R = angles_to_eulermat(0, 0, pi/2)
        >>> p = array([[0], [0], [0]])
        >>> u = array([[0.1],[0.3],[0.5]])
        >>> draw_riptide3D(ax, p, R, u, 0.7)
    """
    u = u.flatten()
    R = ToH(R)
    T = tran3H(*p[0:3, 0])
    flap = add1(array([[-1, 0, 0, -1, -1], [0, 0, 0, 0, 0], [0, 0, 1, 1, 0]]))
    flap1 = tran3H(0, 0, 1) @ rot3H(0, 0, u[0]) @ flap
    flap2 = rot3H(2 * pi / 3, 0, 0) @ tran3H(0, 0, 1) @ rot3H(0, 0, u[1]) @ flap
    flap3 = rot3H(-2 * pi / 3, 0, 0) @ tran3H(0, 0, 1) @ rot3H(0, 0, u[2]) @ flap
    Ca = hstack((circle3H(1.5), [[1.5, -1.5], [0, 0], [0, 0], [1, 1]]))  # the disc + the blades
    C0 = tran3H(-1, 0, 0) @ rot3H(0, 1.57, 0) @ eulerH(0, 0, alpha) @ Ca  # we rotate the blades
    draw3H(ax, T @ R @ C0, 'green')
    draw3H(ax, T @ R @ flap1, 'red')
    draw3H(ax, T @ R @ flap2, 'magenta')
    draw3H(ax, T @ R @ flap3, 'magenta')
    M = T @ R @ cylinder3H(1, 10)
    ax.plot(M[0], M[1], 1 * M[2], color='orange')
    ax.plot(M[0], M[1], 0 * M[2], color='grey')
    pause(0.001)

def draw_riptide(ax : Axes3D, x : np.ndarray, u : np.ndarray, alpha : float):
    """Draw on ax Figure a riptide with state vector x.
    You can also set the angles of the rudders with u and the angle of the blade with alpha.

    Args:
        ax (Axes3D): Figure
        x (np.ndarray): Vertical Numpy array with the state vector of the riptide
        u (np.ndarray): Vertical Numpy array with the angles of the rudders
        alpha (float): Angle of the blade
    
    Examples:
        >>> x = array([[0], [0], [0], [0], [0], [pi/2]])
        >>> u = array([[0.1],[0.3],[0.5]])
        >>> draw_riptide(ax, x, u, 0.7)
    """
    R = angles_to_eulermat(*x[3:6, 0])
    p = array([[x[0, 0]], [x[1, 0]], [x[2, 0]]])
    draw_riptide3D(ax, p, R, u, alpha)

def plot2D(M : np.ndarray, col='black', w=1):
    """Draw on a 2D figure a segment with starting point first column and second point second column.

    Args:
        M (np.ndarray): Matrix containing the coordinates of the points. First line are x coordinates and second line are y coordinates.
        col (str, optional): Color of the plot. Defaults to 'black'.
        w (int, optional): Width of the plot. Defaults to 1.
    
    Examples:
        >>> M = array([[1, 2, 7], [4, 5, 1]])
        >>> plot2D(M, 'red', 2)
    """
    plot(M[0, :], M[1, :], col, linewidth=w)

def plot3D(ax : Axes3D, M : np.ndarray, col='black', w=1):
    """Draw on a 3D figure a segment with starting point inside the first column and second point inside the second column.

    Args:
        ax (Axes3D): Figure
        M (np.ndarray): Matrix containing the coordinates of the points
        col (str, optional): Color of the plot. Defaults to 'black'.
        w (int, optional): Width of the plot. Defaults to 1.
        
    Examples:
        >>> M = array([[1, 2, 7], [4, 5, 1], [4, 0, 1]])
        >>> plot3D(ax, M, 'red', 2)
    """
    ax.plot(M[0, :], M[1, :], M[2, :], col, linewidth=w)

def draw_segment(a : np.ndarray, b : np.ndarray, col='darkblue', w=1):
    """Draw a segment between point a and point b. Both points are represented by vertical Numpy array.

    Args:
        a (np.ndarray): Vector containing the coordinates of the first point.
        b (np.ndarray): Vector containing the coordinates of the second point.
        col (str, optional): Color of the plot. Defaults to 'darkblue'.
        w (int, optional): Width of the plot. Defaults to 1.
    
    Examples:
        >>> a = array([[1], [2]])
        >>> b = array([[7], [4]])
        >>> draw_segment(a, b, 'red', 2)
    """
    plot2D(hstack((a, b)), col, w)

def draw_disk(ax : Axes, c : np.ndarray, r : float, col : str, alph=0.7, w=1):
    """Draw a disk on ax Figure with center c, radius r and color col.

    Args:
        ax (Axes): Figure.
        c (np.ndarray): Center of the disk.
        r (float): Radius of the disk.
        col (str): Color of the disk.
        alph (float, optional): Transparency. Defaults to 0.7.
        w (int, optional): Linewidth. Defaults to 1.
    
    Examples:
        >>> c = array([[1], [2]])
        >>> draw_disk(ax,c,0.5,"blue")
    """
    e = Ellipse(xy=c, width=2 * r, height=2 * r, angle=0, linewidth=w)
    ax.add_artist(e)
    e.set_clip_box(ax.bbox)
    e.set_alpha(alph)
    e.set_facecolor(col)

def draw_box(ax : Axes, x1 : float, x2 : float, y1 : float, y2 : float, col : str):
    """Draw a box on ax Figure with coordinates x1, x2, y1 and y2 and color col.

    Args:
        ax (Axes): Figure
        x1 (float): First x coordinate
        x2 (float): Second x coordinate
        y1 (float): First y coordinate
        y2 (float): Second y coordinate
        col (str): Color of the box
    
    Examples:
        >>> draw_box(ax, 0, 5, 0, 5, "blue")
    """
    c = array([[x1], [y1]])
    rect = Rectangle(c, width=x2 - x1, height=y2 - y1, angle=0)
    rect.set_facecolor(array([0.4, 0.3, 0.6]))
    ax.add_patch(rect)
    rect.set_clip_box(ax.bbox)
    rect.set_alpha(0.7)
    rect.set_facecolor(col)

def draw_polygon(ax : Axes, P : np.ndarray, col : str):
    """Draw a polygon on ax Figure with coordinates P and color col.

    Args:
        ax (Axes): Figure
        P (np.ndarray): Numpy array with shape Nx2
        col (str): Color of the polygon
    
    Examples:
        >>> draw_polygon(ax, array([[1, 2], [-4, -5], [5, -5], [7, 8]]), "blue")
    """
    patches = []
    patches.append(Polygon(P, True))
    p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.4, color=col)
    ax.add_collection(p)

def draw_arc(c : np.ndarray, a : np.ndarray, theta : float, col : str):
    """Draw an arc on ax Figure with center c, x coordinates of the beginning of the arc and radius in a, 
    and angle theta.

    Args:
        c (np.ndarray): Center of the arc
        a (np.ndarray): x coordinates of the beginning of the arc and radius
        theta (float): Angle of the arc
        col (str): Color of the arc
    
    Examples:
        >>> draw_arc(array([[4], [3]]), array([[-2], [5]]), -pi/2, 'red')
    """
    s = arange(0, abs(theta), 0.01)
    s = sign(theta) * s
    d = a - c
    r = norm(d)
    alpha = angle(d)
    w = c @ ones((1, size(s))) + r * array([[cos(alpha), -sin(alpha)], [sin(alpha), cos(alpha)]]) @ array(
        [cos(s), sin(s)])
    plot2D(w, col, 3)

def draw_pie(ax : Axes, c : np.ndarray, rho1 : float, rho2 : float, theta1 : float, theta2 : float, col : str):
    """Draw a pie on ax Figure with center c, radius rho1 and rho2, angles theta1 and theta2 and color col.
    FIXME: This function is not working properly.

    Args:
        ax (Axes): Figure
        c (np.ndarray): Center of the pie
        rho1 (float): First radius
        rho2 (float): Second radius
        theta1 (float): First angle
        theta2 (float): Second angle
        col (str): Color of the pie
        
    Examples:
        >>> draw_pie(ax, array([[4], [3]]), 0.5, 2, -pi / 4, pi / 2, 'red')
    """
    n = 12
    W0 = array([[rho1 * np.cos(theta1)], [rho1 * np.sin(theta1)]])
    W = W0
    dtheta = (theta2 - theta1) / n
    R = array([[np.cos(dtheta), -np.sin(dtheta)], [np.sin(dtheta), np.cos(dtheta)]])
    for i in range(n + 1):
        W0 = R @ W0
        W = hstack((W, c + W0))
    W0 = [[rho2 * np.cos(theta2)], [rho2 * np.sin(theta2)]]
    R = array([[np.cos(dtheta), np.sin(dtheta)], [-np.sin(dtheta), np.cos(dtheta)]])
    for i in range(n + 1):
        W0 = R @ W0
        W = hstack((W, c + W0))
    draw_polygon(ax, W.T, col)

def draw_arrow(x : float, y : float, theta : float, L : float, col : str):
    """Draw an arrow on ax Figure with center (x,y), orientation theta, length L and color col.

    Args:
        x (float): x coordinate of the center of the arrow
        y (float): y coordinate of the center of the arrow
        theta (float): orientation of the arrow
        L (float): length of the arrow
        col (str): color of the arrow
    
    Examples:
        >>> draw_arrow(0, 0, pi / 4, 5, 'red')
    """
    e = 0.2
    M1 = L * array([[0, 1, 1 - e, 1, 1 - e], [0, 0, -e, 0, e]])
    M = np.append(M1, [[1, 1, 1, 1, 1]], axis=0)
    R = array([[cos(theta), -sin(theta), x], [sin(theta), cos(theta), y], [0, 0, 1]])
    plot2D(R @ M, col)

def draw_sailboat(x : np.ndarray, δs : float, δr : float, psi : float, awind : float):
    """Draw a sailboat on ax Figure with state vector x, sail angle δs, rudder angle δr, wind angle psi and wind speed awind.

    Args:
        x (np.ndarray): State vector of the sailboat (mx, my, theta, v, w)
        δs (float): Sail angle
        δr (float): Rudder angle
        psi (float): Wind angle
        awind (float): Wind speed
    
    Examples:
        >>> draw_sailboat(array([[1], [2], [3], [4], [5]]), pi / 4, pi / 3, pi / 4, 1)
    """
    mx, my, theta, v, w = list(x[0:5, 0])
    hull = add1(array([[-1, 5, 7, 7, 5, -1, -1, -1], [-2, -2, -1, 1, 2, 2, -2, -2]]))
    sail = array([[-7, 0], [0, 0], [1, 1]])
    rudder = array([[-1, 1], [0, 0], [1, 1]])
    R = tran2H(mx, my) @ rot2H(theta)
    Rs = tran2H(3, 0) @ rot2H(δs)
    Rr = tran2H(-1, 0) @ rot2H(δr)
    draw_arrow(float(x[0]) + 5, float(x[1]), psi, 5 * awind, 'red')
    plot2D(R @ hull, 'black')
    plot2D(R @ Rs @ sail, 'red', 2)
    plot2D(R @ Rr @ rudder, 'red', 2)

def draw_tank(x : np.ndarray, col='darkblue', r=1, w=2):
    """Draw a tank on ax Figure with state vector x.

    Args:
        x (np.ndarray): State vector of the tank (mx, my, theta)
        col (str, optional): _description_. Defaults to 'darkblue'.
        r (int, optional): _description_. Defaults to 1.
        w (int, optional): _description_. Defaults to 2.
    
    Examples:
        >>> draw_tank(array([[1], [2], [3]]), 'blue', 3)
    """
    mx, my, theta = list(x[0:3, 0])
    M = r * array(
        [[1, -1, 0, 0, -1, -1, 0, 0, -1, 1, 0, 0, 3, 3, 0], [-2, -2, -2, -1, -1, 1, 1, 2, 2, 2, 2, 1, 0.5, -0.5, -1]])
    M = add1(M)
    plot2D(tran2H(mx, my) @ rot2H(theta) @ M, col, w)

def draw_invpend(ax : Axes, x : np.ndarray):
    """Draw an inverted pendulum on ax Figure with state vector x.

    Args:
        ax (Axes): Figure
        x (np.ndarray): State vector of the inverted pendulum (s, theta)
    
    Examples:
        >>> draw_invpend(ax, array([[1], [2]]))
    """
    s, theta = x[0, 0], x[1, 0]
    draw_box(ax, s - 0.7, s + 0.7, -0.25, 0, 'blue')
    plot([s, s - sin(theta)], [0, cos(theta)], 'magenta', linewidth=2)

def draw_car(x : np.ndarray, col='darkblue', L=1, w=2):
    """Draw a car on ax Figure with state vector x.

    Args:
        x (np.ndarray): State vector of the car (mx, my, theta, v, δ)
        col (str, optional): Color of the car . Defaults to 'darkblue'.
        L (int, optional): Length of the car. Defaults to 1.
        w (int, optional): Width of the car. Defaults to 2.
    
    Examples:
        >>> draw_car(array([[1], [2], [3], [4], [0.5]]), 'blue', 3)
    """
    mx, my, theta, v, δ = list(x[0:5, 0])
    M = add1(L * array([[-0.3, 1.3, 1.6, 1.6, 1.3, -0.3, -0.3, -0.3, 0, 0, -0.3, 0.3, 0, 0, -0.3, 0.3, 0, 0, 1, 1, 1],
                        [-0.7, -0.7, -0.3, 0.3, 0.7, 0.7, -0.7, -0.7, -0.7, -1, -1, -1, -1, 1, 1, 1, 1, 0.7, 0.7, 1,
                         -1]]))
    R = tran2H(mx, my) @ rot2H(theta)
    W = add1(L * array([[-0.3, 0.3], [0, 0]]))  # Front Wheel
    plot2D(R @ M, col, w)
    plot2D(R @ tran2H(L, L) @ rot2H(δ) @ W, col, 2)
    plot2D(R @ tran2H(L, -L) @ rot2H(δ) @ W, col, 2)

###### KALMAN #######

def toNDArray(M : Any) -> np.ndarray:
    """Transform an element into a Numpy array.

    Args:
        M (Any): Element to transform

    Returns:
        np.ndarray: Numpy array
    
    Examples:
        >>> M = toNDArray(1)
        array([[1]])
    """
    if isinstance(M, (float, int)):
        return array([[M]])
    return M

def mvnrnd(xbar : np.ndarray, Γ : np.ndarray, n : int) -> np.ndarray:
    """Return n random vectors from a multivariate normal distribution with mean xbar and covariance matrix Γ.

    Args:
        xbar (np.ndarray): Mean of the distribution
        n (int): Number of random vectors to return

    Returns:
        np.ndarray: Numpy array with shape (len(xbar), n)
    
    Examples:
        >>> X = mvnrnd(array([[1], [3]]), array([[1, 0], [0, 1]]), 4)
        array([[0.79689134, 2.04727364, 1.03853946, 1.30858353],
               [3.9647011 , 5.00070669, 5.24270302, 3.89430702]])
    """
    X = randn(2, n)
    X = (xbar @ ones((1, n))) + sqrtm(Γ) @ X
    return (X)

def mvnrnd1(G : np.ndarray) -> np.ndarray:
    """Return a random vector from a multivariate normal distribution with covariance matrix G.

    Args:
        G (np.ndarray): Covariance matrix

    Returns:
        np.ndarray: Numpy array with shape (len(G), 1)
    
    Examples:
        >>> X1 = mvnrnd1(array([[1, 0], [0, 1]]))
        array([[1.14333869],
               [1.10920368]])
    """
    G = toNDArray(G)
    n = len(G)
    x = array([[0]] * n)
    return (_mvnrnd2(x, G))

def _mvnrnd2(x : np.ndarray, G : np.ndarray) -> np.ndarray:
    """Return a random vector from a multivariate normal distribution with covariance matrix G.

    Args:
        x (np.ndarray): Empty vector
        G (np.ndarray): Covariance matrix

    Returns:
        np.ndarray: Numpy array with shape (len(G), 1)
    
    Examples:
        >>> X2 = _mvnrnd2(array([[1], [2]]), array([[1, 0], [0, 1]]))
        array([[1.45483459],
               [0.59342112]])
    """
    n = len(x)
    x1 = x.reshape(n)
    y = np.random.multivariate_normal(x1, G).reshape(n, 1)
    return (y)

def kalman_correc(x0 : np.ndarray, Γ0 : np.ndarray, y : np.ndarray, Γβ : np.ndarray, C : np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Returns the correction of the state vector x0 and the covariance matrix Γ0.

    Args:
        x0 (np.ndarray): Prediction of the state vector
        Γ0 (np.ndarray): Prediction of the covariance matrix
        y (np.ndarray): Sensor measurement
        Γβ (np.ndarray): Sensor noise covariance matrix
        C (np.ndarray): Conversion matrix

    Returns:
        Tuple[np.ndarray, np.ndarray]: Correction of the state vector and the covariance matrix
    
    Examples:
        >>> x0 = array([[1], [2]])
        >>> Γ0 = array([[1, 0], [0, 1]])
        >>> y = array([[1], [2.5]])
        >>> Γβ = array([[0.5, 0], [0, 0.7]])
        >>> C = array([[1, 0], [0, 1]])
        >>> xup, Gup = kalman_correc(x0, Γ0, y, Γβ, C)
        array([[1.        ],
               [2.29411765]])
        array([[0.33333333, 0.        ],
               [0.        , 0.41176471]])
    """
    S = C @ Γ0 @ C.T + Γβ
    K = Γ0 @ C.T @ inv(S)
    ytilde = y - C @ x0
    Gup = (eye(len(x0)) - K @ C) @ Γ0
    xup = x0 + K @ ytilde
    return (xup, Gup)

def kalman_predict(xup : np.ndarray, Gup : np.ndarray, u : np.ndarray, Γalpha : np.ndarray, A : np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Returns the prediction of the state vector x1 and the covariance matrix Γ1.

    Args:
        xup (np.ndarray): Previous state vector
        Gup (np.ndarray): Previous covariance matrix
        u (np.ndarray): Control variable matrix
        Γalpha (np.ndarray): Process noise covariance matrix
        A (np.ndarray): Adaptation matrix

    Returns:
        Tuple[np.ndarray, np.ndarray]: Prediction of the state vector and the covariance matrix
    
    Examples:
        >>> xup = array([[1.        ],
                         [2.29411765]])
        >>> Gup = array([[0.33333333, 0.        ],
                         [0.        , 0.41176471]])
        >>> u = array([[1], [2]])
        >>> Γalpha = array([[0.5, 0], [0, 0.7]])
        >>> A = array([[1, 0], [0, 1]])
        >>> x1, Γ1 = kalman_predict(xup, Gup, u, Γalpha, A)
        array([[2.        ],
               [4.29411765]])
        array([[0.83333333, 0.        ],
               [0.        , 1.11176471]])
    """
    Γ1 = A @ Gup @ A.T + Γalpha
    x1 = A @ xup + u
    return (x1, Γ1)

def kalman(x0 : np.ndarray, Γ0 : np.ndarray, u : np.ndarray, y : np.ndarray, Γalpha : np.ndarray, Γβ : np.ndarray, A : np.ndarray, C : np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Kalman Filter. Returns the state matrix and the process covariance matrix.

    Args:
        x0 (np.ndarray): Initial state vector
        Γ0 (np.ndarray): Initial covariance matrix
        u (np.ndarray): Control variable matrix
        y (np.ndarray): Sensor measurement
        Γalpha (np.ndarray): Process noise covariance matrix
        Γβ (np.ndarray): Sensor noise covariance matrix
        A (np.ndarray): Adaptation matrix
        C (np.ndarray): Conversion matrix

    Returns:
        Tuple[np.ndarray, np.ndarray]: State matrix and process covariance matrix.
    
    Examples:
        >>> x0 = array([[1], [2]])
        >>> Γ0 = array([[1, 0], [0, 1]])
        >>> u = array([[1], [2]])
        >>> y = array([[1], [2.5]])
        >>> Γalpha = array([[0.5, 0], [0, 0.7]])
        >>> Γβ = array([[0.5, 0], [0, 0.7]])
        >>> A = array([[1, 0], [0, 1]])
        >>> C = array([[1, 0], [0, 1]])
        >>> x1, Γ1 = kalman(x0, Γ0, u, y, Γalpha, Γβ, A, C)
        array([[2.        ],
               [4.29411765]])
        array([[0.83333333, 0.        ],
               [0.        , 1.11176471]])
    """
    xup, Gup = kalman_correc(x0, Γ0, y, Γβ, C)
    x1, Γ1 = kalman_predict(xup, Gup, u, Γalpha, A)
    return (x1, Γ1)

def loadcsv(file: str) -> np.ndarray:
    """Load a csv file and return a Numpy array.
    
    Args:
        file (str): Path to the csv file
    
    Examples:
        >>> D = loadcsv("data.csv")
        array([[1. , 2. , 3. ],
               [4. , 5. , 6. ]])
    """
    with open(file, 'r', newline='', encoding='utf8') as fichier:
        D = fichier.read().split("\n")
        fichier.close()
        for i in range(len(D)):
            D[i] = D[i].split(";")
        D = array([[float(elt) for elt in Ligne] for Ligne in D])
        return D

def draw_field(f: Any, xmin: float, xmax: float, ymin: float, ymax: float, a: float):
    """Draw a vector field on ax Figure with f function.
    
    Args:
        f (Any): Function
        xmin (float): Minimum x coordinate
        xmax (float): Maximum x coordinate
        ymin (float): Minimum y coordinate
        ymax (float): Maximum y coordinate
        a (float): Step
    
    Examples:
        >>> s = 10
        >>> def f(x1,x2): return x2,-x1
        >>> ax = init_figure(-s, s, -s, s)
        >>> draw_field(f, -s, s, -s, s, 1)
        >>> show()
    """
    Mx    = arange(xmin,xmax,a)
    My    = arange(ymin,ymax,a)
    X1,X2 = meshgrid(Mx,My)
    VX,VY = f(X1,X2) 
    R = sqrt(VX**2+VY**2)
    quiver(Mx,My,VX/R,VY/R)

def _draw_ellipse0(ax : Axes, c : np.ndarray, Γ : np.ndarray, a : float, col : str, coledge='black'):  # classical ellipse (x-c)T * invΓ * (x-c) <a^2
    """Draw an ellipse on ax Figure with center c, covariance matrix Γ, a confidence interval and color col.

    Args:
        ax (Axes): Figure
        c (np.ndarray): Center of the ellipse
        Γ (np.ndarray): Covariance matrix
        a (float): Confidence interval
        col (str): Color of the ellipse
        coledge (str, optional): Color of the edge of the ellipse. Defaults to 'black'.
        
    Examples:
        >>> ax = init_figure(-15, 15, -15, 15)
        >>> _draw_ellipse0(ax,array([[1],[2]]),eye(2),0.80,[0.5,0.6,0.7])
        >>> show()

    """
    A = a * sqrtm(Γ)
    w, v = eig(A)
    v1 = array([[v[0, 0]], [v[1, 0]]])
    v2 = array([[v[0, 1]], [v[1, 1]]])
    f1 = A @ v1
    f2 = A @ v2
    phi = (arctan2(v1[1, 0], v1[0, 0]))
    alpha = phi * 180 / 3.14
    e = Ellipse(xy=c, width=2 * norm(f1), height=2 * norm(f2), angle=alpha)
    ax.add_artist(e)
    e.set_clip_box(ax.bbox)

    e.set_alpha(0.7)
    e.set_facecolor(col)
    e.set_edgecolor(coledge)
    # e.set_fill(False)
    # e.set_alpha(1)

def draw_ellipse_cov(ax : Axes, c : np.ndarray, Γ : np.ndarray, η : float, col : str, coledge='black'):  # Gaussian confidence ellipse with artist
    """Draw a Gaussian confidence ellipse on ax Figure with center c, covariance matrix Γ, η confidence interval and color col.

    Args:
        ax (Axes): Figure
        c (np.ndarray): Center of the ellipse
        Γ (np.ndarray): Covariance matrix
        η (float): Confidence interval
        col (str): Color of the ellipse
        coledge (str, optional): Color of the edge of the ellipse. Defaults to 'black'.
        
    Examples:
        >>> ax = init_figure(-15, 15, -15, 15)
        >>> draw_ellipse_cov(ax,array([[1],[2]]),eye(2),0.9,[0.5,0.6,0.7])
        >>> show()
    """
    if norm(Γ) == 0:
        Γ = Γ + 0.001 * eye(len(Γ[1, :]))
    a = sqrt(-2 * log(1 - η))
    _draw_ellipse0(ax, c, Γ, a, col, coledge)


###### DEMOS #######


def demo_draw():
    """Draw a demo figure with a tank, a car, an ellipse, a rectangle, a polygon, a disk and an arc.
    """
    ax = init_figure(-15, 15, -15, 15)

    c = array([[5], [0]])
    e = Ellipse(xy=c, width=13.0, height=2.0, angle=45)
    ax.add_artist(e)
    e.set_clip_box(ax.bbox)
    e.set_alpha(0.9)
    e.set_facecolor(array([0.7, 0.3, 0.6]))

    rect = Rectangle((1, 1), width=5, height=3)
    rect.set_facecolor(array([0.4, 0.3, 0.6]))
    ax.add_patch(rect)

    draw_tank(array([[-7], [5], [1]]))
    draw_tank(array([[-7], [5], [1]]), 'red', 0.2)
    draw_car(array([[1], [2], [3], [4], [0.5]]), 'blue', 3)

    c = array([[-2], [-3]])
    G = array([[2, -1], [-1, 4]])
    draw_ellipse_cov(ax, c, G, 0.9, [0.8, 0.8, 1])
    P = array([[5, -3], [9, -10], [7, -4], [7, -6]])
    draw_polygon(ax, P, 'green')
    draw_disk(ax, array([[-8], [-8]]), 2, "blue")
    draw_arc(array([[0], [5]]), array([[4], [6]]), 2, 'red')
    show()  # only at the end. Otherwize, it closes the figure in a terminal mode

def demo_animation():
    """Draw a demo animation with a tank and an ellipse moving through the screen.
    """
    ax = init_figure(-15, 15, -15, 15)
    for t in arange(0, 5, 0.1):
        clear(ax)
        draw_car(array([[t], [2], [3 + t], [4], [5 + t]]), 'blue', 3)
        c = array([[-2 + 2 * t], [-3]])
        G = array([[2 + t, -1], [-1, 4 + t]])
        draw_ellipse_cov(ax, c, G, 0.9, [0.8, 0.8, 1])
    show()

def demo_tank(x1 : float, x2 : float, x3 : float, x4 : float):
    """Draw a tank and a trolley with state vector x1, x2, x3 and x4.

    Args:
        x1 (float): mx
        x2 (float): my
        x3 (float): theta
        x4 (float): δ, angle of the trolley in radian
        
    Examples:
        >>> demo_tank(1,2,pi/4,pi/2)
    """
    ax = init_figure(0, 3, 0, 3)
    r = 0.07
    M = add1(r * np.array(
        [[1, -1, 0, 0, -1, -1, 0, 0, -1, 1, 0, 0, 4, 4, 0], [-3, -3, -3, -2, -2, 2, 2, 3, 3, 3, 3, 2, 1, -1, -2]]))
    R1 = tran2H(x1, x2) @ rot2H(x3)
    MT = add1(r * np.array([[1, -1, 0, 0, 1, -1, 0, 0, 1 / r], [-3, -3, -3, 3, 3, 3, 3, 0, 0]]))
    plot2D(R1 @ M, 'blue', 2)
    plot2D(R1 @ rot2H(x4 - x3) @ tran2H(-1, 0) @ MT, 'magenta', 2)
    show()

def demo_random():
    """Draw a demo figure with random points and the corresponding covarience ellipse.
    """
    N = 1000
    xbar = array([[1], [2]])
    Γx = array([[3, 1], [1, 3]])
    X = randn(2, N)
    Y = rand(2, 3)
    print("Y=", Y)
    X = (xbar @ ones((1, N))) + sqrtm(Γx) @ X
    xbar_ = mean(X, axis=1)
    Xtilde = X - xbar @ ones((1, N))
    Γx_ = (Xtilde @ Xtilde.T) / N
    ax = init_figure(-20, 20, -20, 20)
    draw_ellipse_cov(ax, xbar_, Γx_, 0.9, [1, 0.8, 0.8])
    pause(0.5)
    ax.scatter(*X)
    pause(1)
    plot()

def demo_field():
    """Draw a demo figure with a vector field.
    """
    def f(x1, x2): 
        return -(x1 ** 3 + x2 ** 2 * x1 - x1 + x2), -(x2 ** 3 + x1 ** 2 * x2 - x1 - x2)

    xmin, xmax, ymin, ymax = -2.5, 2.5, -2.5, 2.5
    ax = init_figure(xmin, xmax, ymin, ymax)
    draw_field(f, xmin, xmax, ymin, ymax, 0.3)
    pause(1)

if __name__ == "__main__":
    print('You are using Roblib v2. Welcome!')

    # x = scalarprod(array([[1], [2]]), array([[3], [4]]))
    # R = angles_to_eulermat(0,0,pi/2)
    # (phi, theta, psi) = eulermat_to_angles(R)
    # R_dot = eulerderivative(pi/2, -pi)
    # psi = angle(array([[1], [2]]))
    # M = add1(array([[1, 2], [3, 4]]))
    # L = tolist(array([[1], [2]]))
    # A = adjoint(array([[1], [2], [3]]))
    # A_inv = adjoint_inv(A)
    # H = ToH(array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
    # e = expw(array([[1], [2], [3]]))
    # eh = expwH(array([[1], [2], [3]]))
    # L = logw(array([[-0.69492056 ,  0.71352099 , 0.08929286],
    #                 [-0.19200697 , -0.30378504 , 0.93319235],
    #                 [ 0.69297817 ,  0.6313497  , 0.34810748]]))
    # M = Rlatlong(48.418859, -4.473766)
    # M = tran2H(1, 2)
    # R = rot2w(M)
    # M = rot2H(pi/2)
    # R = eulerH(0, 0, pi/2)
    # M = tran3H(1, 2, 3)
    # M = rot3H(pi/2, 0, 0)
    # M = angle3d(array([[1], [2], [3]]), array([[4], [5], [6]]))
    # M = projSO3(array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
    # x = latlong2cart(6371000, 48.418859, -4.473766)
    # r, lx, ly= cart2latlong(x[0,0], x[1,0], x[2,0])
    # x = sawtooth(3*pi + pi/2)
    # A = np.array([[ 0,  7/3.,  0,   0   ],
    #               [ 0,   0,    0,  7/9. ],
    #               [ 0,   0,    0,   0   ],
    #               [ 0,   0,    0,   0   ]])
    # B = np.array([[ 0,  0 ],
    #               [ 0,  0 ],
    #               [ 1,  0 ],
    #               [ 0,  1 ]])
    # P = np.array([-3, -1, -2-1j, -2+1j]) / 3.
    # K = place(A,B,P)

    # ax = init_figure()
    # clear(ax)
    # show()
    # ax = axis3D()
    # clean3D(ax)
    # draw_arrow3D(ax, 0, 0, 0, 1, 1, 1, "red")
    # R = rot3H(0, 0, 0)
    # draw_axis3D(ax, 0, 0, 0, R)
    # cube = cube3H()
    # auv = auv3H()
    # wheel = wheel3H(1, 12)
    # circle = circle3H(1, 20)
    # earth = earth3H(10, pi/10)
    # cylinder = cylinder3H(1, 10, 25)
    # draw3H(ax, auv, 'blue')
    # R = angles_to_eulermat(0, pi/2, 0)
    # draw_earth3D(ax, 10, R, col='blue')
    # draw_wheel3D(ax, 0, 0, 0, 0, pi/2, 0, r=3, col='red')
    # R = angles_to_eulermat(0, pi/2, 0)
    # p = array([[0], [0], [0]])
    # draw_robot3D(ax, p, R, col='red')
    # draw_auv3D(ax, 0, 0, 0, 0, pi/2, 0, col='red', size=3)
    # R = angles_to_eulermat(0, pi/2, 0)
    # p = array([[0], [0], [0]])
    # alpha = array([[0.1],[0.3],[0.5],[0.7]])
    # draw_quadrotor3D(ax, p, R, alpha, l=3)
    # draw_riptide3D(ax, array([[0], [0], [0]]), angles_to_eulermat(0, 0, 0), array([[0.1],[0.3],[0.5]]), 2)
    # x = array([[0], [0], [0], [0], [0], [pi/2]])
    # u = array([[0.1], [0.3], [0.5]])
    # draw_riptide(ax, x, u, 0.7)
    # M = array([[1, 2, 7], [4, 5, 1]])
    # plot2D(M, 'red', 2)
    # M = array([[1, 2, 7], [4, 5, 1], [4, 0, 1]])
    # plot3D(ax, M, 'red', 2)
    # draw_segment(array([[1], [2]]), array([[7], [4]]), 'red', 2)
    # ax = init_figure()
    # draw_disk(ax,array([[1], [2]]),0.5,"blue")
    # draw_box(ax, 0, 5, 0, 5, "blue")
    # draw_polygon(ax, array([[1, 2], [-4, -5], [5, -5], [7, 8]]), "blue")
    # draw_arc(array([[4], [3]]), array([[-2], [5]]), -pi/2, 'red')
    # draw_pie(ax, array([[4], [3]]), 0.5, 2, -pi / 4, pi / 2, 'red')
    # draw_arrow(0, 0, pi / 4, 5, 'red')
    # draw_sailboat(array([[1], [2], [3], [4], [5]]), pi / 4, pi / 3, pi / 4, 1)
    # draw_tank(array([[1], [2], [3]]), 'blue', 3)
    # draw_invpend(ax, array([[1], [2]]))
    # draw_car(array([[1], [2], [3], [4], [0.5]]), 'blue', 3)
    show()

    # M = toNDArray(1)
    # X = mvnrnd(array([[1], [3]]), array([[1, 0], [0, 1]]), 4)
    # X1 = mvnrnd1(array([[1, 0], [0, 1]]))
    # X2 = _mvnrnd2(array([[1], [2]]), array([[1, 0], [0, 1]]))
    # x0 = array([[1], [2]])
    # Γ0 = array([[1, 0], [0, 1]])
    # y = array([[1], [2.5]])
    # Γβ = array([[0.5, 0], [0, 0.7]])
    # C = array([[1, 0], [0, 1]])
    # xup, Gup = kalman_correc(x0, Γ0, y, Γβ, C)
    # x1, Γ1 = kalman_predict(xup, Gup, array([[1], [2]]), array([[0.5, 0], [0, 0.7]]), array([[1, 0], [0, 1]]))
    # x1, Γ1 = kalman(x0, Γ0, array([[1], [2]]), array([[1], [2.5]]), array([[0.5, 0], [0, 0.7]]), array([[0.5, 0], [0, 0.7]]), array([[1, 0], [0, 1]]), array([[1, 0], [0, 1]]))
    # ax = init_figure(-15, 15, -15, 15)
    # s = 10
    # def f(x1,x2): return x2,-x1
    # draw_field(f, -s, s, -s, s, 1)
    # _draw_ellipse0(ax,array([[1],[2]]),eye(2),0.80,[0.5,0.6,0.7])
    # draw_ellipse_cov(ax,array([[1],[2]]),eye(2),0.9,[0.5,0.6,0.7])
    # show()
    
    # demo_draw()
    # demo_animation()
    # demo_tank(1,2,pi/4,pi/2)
    # demo_random()
    # demo_field()
