from roblib import array, sqrt, eye, sin, cos, init_figure, clear, draw_invpend, mvnrnd1, pause, arange  # available at https://www.ensta-bretagne.fr/jaulin/roblib.py
import numpy
from numpy.linalg import inv
import cvxopt
import picos as pic

mc,l,g,mr = 5,1,9.81,1
dt = 0.02

x = array([[0,0.5,0,0]]).T
Γα = (sqrt(dt)*(10**-3))**2*eye(4)

def f(x,u):
    s,θ,ds,dθ=x[0,0],x[1,0],x[2,0],x[3,0]
    dds=(mr*sin(θ)*(g*cos(θ)- l*dθ**2) + u)/(mc+mr*sin(θ)**2)
    ddθ= (sin(θ)*((mr+mc)*g - mr*l*dθ**2*cos(θ)) + cos(θ)*u)/ (l*(mc+mr*sin(θ)**2))
    return array([[ds],[dθ],[dds],[ddθ]])

def k_mat():
    # System parameters 
    A_mat = numpy.matrix([                                                                                        
    [0.0, 0.0, 1.0, 0.0],                                                                                            
    [0.0, 0.0, 0.0, 1.0],
    [0.0, mr*g/mc, 0.0, 0.0],
    [0.0, (mc+mr)*g/(mc*l), 0.0, 0.0]
    ])
    B_mat = numpy.matrix([                                                                                        
    [0.0],                                                                                            
    [0.0],
    [1/mc],
    [1/(mc*l)]
    ])                                                                       
    # Create problem instance.                                                                                    
    prob = pic.Problem()                                                                                          
    # Define variables.                                                                                          
    t = pic.RealVariable('t', 1)                                                                                
    Q = pic.SymmetricVariable('Q', 4)
    Y = pic.RealVariable('Y', (1, 4))
    # Define parameters.                                                                                          
    A = pic.Constant('A', A_mat)                                                                                
    B = pic.Constant('B', B_mat)                                                                                
    I = pic.Constant('I', numpy.identity(4))                                                                    
    # Constraints                                                                                                
    prob.add_constraint(Q >> 0)
    prob.add_constraint(t >> 0)
    prob.add_constraint(Q * A.T - Y.T * B.T + A * Q - B * Y - t * I << 0)                                                            
    # Set objective and solve.                                                                                    
    prob.set_objective('min', t)                                                                                  
    prob.solve()

    return(numpy.matrix(Y)*inv(numpy.matrix(Q)))

ax=init_figure(-3,3,-3,3)

for t in arange(0,50,dt) :
    clear(ax)
    draw_invpend(ax,x)
    u = float(-k_mat()@x)
    α=mvnrnd1(Γα)
    x = x + dt*f(x,u)+α
pause(1)
